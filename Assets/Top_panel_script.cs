﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Top_panel_script : MonoBehaviour
{

    public static Top_panel_script Instance;

    public GameObject Left_Option_But,Right_Option_but;

    public Text Diamond_value, Gold_value;

    [HideInInspector]
    //0 contact us, 1 Instruction 
    public Sprite[] Right_Option_but_sprites;

    [HideInInspector]
    //0 menu, 1 close
    public Sprite[] Left_Option_but_sprites;





    private void Awake()
    {
        Instance = this;
    }

    public void Set_Menu_but()
    {
        Left_Option_But.GetComponent<Image>().sprite = Left_Option_but_sprites[0];
        Left_Option_But.GetComponent<Button>().onClick.AddListener(() => OnMenuClick());
    }

    private void OnMenuClick()
    {
        //do something when menu clicked
        
    }


    public void Set_Close_but()
    {
        Left_Option_But.GetComponent<Image>().sprite = Left_Option_but_sprites[1];
        Left_Option_But.GetComponent<Button>().onClick.AddListener(() => OnCloseClick());
    }

    private void OnCloseClick()
    {
        //do something on close clicked
        //Canvas_singleton_script.Instance.with
    }


    public void onclick_privacy_policy()
    {
        Application.OpenURL(Setting_API.Instance.privacy_policy);
        //Setting_API.Instance.profile_pic_link
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
