﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gameplay_panel_script : MonoBehaviour
{

    public static Gameplay_panel_script Instance;

    public Text Level_diamond;


    public GameObject chaaracter_anim;



    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Withdraw_panel()
    {
        Canvas_singleton_script.Instance.Top_panel.SetActive(true);
        Canvas_singleton_script.Instance.Withdraw_panel.SetActive(true);
    }
    public void Redeem_panel()
    {
        Canvas_singleton_script.Instance.Top_panel.SetActive(true);
        Canvas_singleton_script.Instance.Redeem_panel.SetActive(true);
    }
}
