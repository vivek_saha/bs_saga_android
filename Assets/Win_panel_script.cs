﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Win_panel_script : MonoBehaviour
{
    public Text Final_diamond;
    public GameObject[] stars;
    public Text level;

    private void OnEnable()
    {
        level.text = "Level " + PlayerPrefs.GetInt("OpenLevel");
        StartCoroutine(stars_count());
    }

    IEnumerator stars_count()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        //mainscript.Instance.stars
        for(int i =0;i< mainscript.Instance.stars;i++)
        {
            stars[i].SetActive(true);
        }
        mainscript.Instance.collect_diamond_value = float.Parse( Final_diamond.text );
    }

    //public void collect_but_click()
    //{
    //}

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
