﻿using SimpleJSON;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class withdraw_skin_dia_Api_script : Singleton<withdraw_skin_dia_Api_script>
{

    string p_id;
    string c_id;
    string rawJson;
    public JSONNode jsonResult;
    public void SEt_update_data(string product_id, string character_id)
    {
        p_id = product_id;
        c_id = character_id;

        Update_data();

    }

    private void Update_data()
    {
        StartCoroutine("Upload");
    }

    IEnumerator Upload()
    {
        WWWForm form = new WWWForm();

        form.AddField("productid",p_id );

        form.AddField("character_id", c_id);


        UnityWebRequest www = UnityWebRequest.Post(Firebase_custome_script.Instance.server_link + "/api/login", form);
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.SendWebRequest();
        Debug.Log("www: " + www.responseCode);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Check_Internet_connection();
        }
        else
        {
            //Scene scene__1 = SceneManager.GetActiveScene();
            //if (scene__1.name != "Gameplay")
                close_Retry_panel();

            rawJson = www.downloadHandler.text;
            Debug.Log("Update_API_data upload complete!" + rawJson);

            GameManager.Instance.nem_popup_fun("Your skin will be available in game within 3-4 working days.");
        }

    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    //Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.Something_wentwrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    //Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.No_internet_Panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.No_internet_Panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }
    public void Retry_connection()
    {
        Update_data();
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        //Notice_panel_script.Instance.notice_Panel.SetActive(false);
        Update_data();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        //Notice_panel_script.Instance.notice_Panel.SetActive(false);
    }

    protected override void OnApplicationQuitCallback()
    {
        //throw new NotImplementedException();
    }

    protected override void OnEnableCallback()
    {
        //throw new NotImplementedException();
    }

}
