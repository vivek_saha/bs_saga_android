﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class splashBG_change_script : MonoBehaviour
{
    public static splashBG_change_script Instance;

    public GameObject Loading_panel;
    public Image Loaded_fill_bar;

    public Sprite[] BG_sprites;
    public Sprite[] BG_front_sprites;

    public Image BG_image;

    public Image BG_front;




    private void Awake()
    {
        Instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        //ipad and iphone resolution set
        if (((float)Screen.height / (float)Screen.width) < 1.7)
        {
            BG_front.sprite = BG_front_sprites[2];
        }
        else if (((float)Screen.height / (float)Screen.width) > 2)
        {
            BG_front.sprite = BG_front_sprites[1];
        }
        else
        {
            BG_front.sprite = BG_front_sprites[0];
        }
        //Loaded_fill_bar.fillAmount = 
        Loaded_fill_bar.DOFillAmount(1f,3f);

    }

    // Update is called once per frame
    void Update()
    {
        //if(Loaded_fill_bar.)
    }

    public void load_map_scene()
    {
        SceneManager.LoadScene("map");
    }
}
