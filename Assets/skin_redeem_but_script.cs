﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

//public enum withdraw_type { Diamond , Robux_Diamond}
public class skin_redeem_but_script : MonoBehaviour
{

    //public withdraw_type wth;   

    public Text Skin_Name, Skin_description, Slider_fill_Text;

    public Image Slider_fill_image;

    public string product_id;

    public Image SkinImage;

    public string skinImage_link;

    public Button redeem_but;

    public int frag_limit;

    // Start is called before the first frame update
    void Start()
    {
        Download_image();
    }


    public void Download_image()
    {
        StartCoroutine(GetTexture());
    }

    IEnumerator GetTexture()
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(Firebase_custome_script.Instance.server_link + skinImage_link);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
            Texture2D myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            SkinImage.sprite = Sprite.Create(myTexture, new Rect(0.0f, 0.0f, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f), 100.0f);
        }
    }



    public void set_fragment_fill()
    {
        Slider_fill_Text.text = PlayerPrefs.GetInt(Skin_Name.text)+" / "+frag_limit;
        Slider_fill_image.fillAmount=   (float)PlayerPrefs.GetInt(Skin_Name.text) / (float)frag_limit;
    }


    public void button_Click()
    {
        redeem_but.onClick.AddListener(() => open_withdraw_redeem_panel());
    }

    private void open_withdraw_redeem_panel()
    {
        //throw new NotImplementedException();
        if (PlayerPrefs.GetInt(Skin_Name.text) >= frag_limit)
        {
            //withdraw_panel
            Canvas_singleton_script.Instance.Popup_withdraw_panel.GetComponent<Popup_withdraw_panel_script>().p_id = product_id;
            Canvas_singleton_script.Instance.Popup_withdraw_panel.SetActive(true);
        }
        else
        {
            Canvas_singleton_script.Instance.FrageMent_skin_open(Skin_Name.text, SkinImage.sprite);
        }
    }

    // Update is called once per frame
    void Update()
    {
        set_fragment_fill();
    }
}
