﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEditor;
//using UnityEngine;
////using static System.Collections.Specialized.BitVector32;

//public enum Section
//{
//    A,
//    B
//}

//public enum SectionA
//{
//    A1,
//    A2,
//    A3
//}

//public enum SectionB
//{
//    B1,
//    B2,
//    B3
//}
//public class Item : MonoBehaviour
//{
//    public Section section;
//    public SectionA sectionA;
//    public SectionB sectionB;

//    public System.Enum Value
//    {
//        get
//        {
//            switch (section)
//            {
//                case Section.A:
//                    return sectionA;
//                case Section.B:
//                    return sectionB;
//                default:
//                    return null;
//            }
//        }
//    }
//}

//[CustomEditor(typeof(Item))]
//public class ItemEditor : Editor
//{
//    SerializedProperty section;
//    SerializedProperty sectionA;
//    SerializedProperty sectionB;

//    void OnEnable()
//    {
//        section = serializedObject.FindProperty("section");
//        sectionA = serializedObject.FindProperty("sectionA");
//        sectionB = serializedObject.FindProperty("sectionB");
//    }

//    public override void OnInspectorGUI()
//    {
//        serializedObject.Update();
//        EditorGUILayout.PropertyField(section);
//        switch ((Section)section.enumValueIndex)
//        {
//            case Section.A:
//                EditorGUILayout.PropertyField(sectionA);
//                break;
//            case Section.B:
//                EditorGUILayout.PropertyField(sectionB);
//                break;
//        }
//        serializedObject.ApplyModifiedProperties();
//    }
//}
