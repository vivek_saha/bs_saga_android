﻿using UnityEngine;

public class Canvas_singleton_script : Singleton<Canvas_singleton_script>
{



    [Header("Panels")]
    public GameObject Withdraw_panel;
    public GameObject Redeem_panel;
    public GameObject Top_panel;
    public GameObject Contact_us_panel;
    public GameObject fragment_panel;
    public GameObject Popup_withdraw_panel; 


    public GameObject origin;

    private void Start()
    {
        Withdraw_panel.GetComponent<Withdraw_panel_script>().generate_but();
        Redeem_panel.GetComponent<Redeem_panel_script>().generate_but();

    }

    protected override void OnApplicationQuitCallback()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnEnableCallback()
    {
        //throw new System.NotImplementedException();
    }

    public void onwithdraw_close()
    {
        Withdraw_panel.SetActive(false);
    }
    public void onredeem_close()
    {
        Redeem_panel.SetActive(false);
    }

    public void Close_click()
    {
        if (Redeem_panel.activeSelf)
        {
            onredeem_close();
        }
        if (Withdraw_panel.activeSelf)
        {
            onwithdraw_close();
        }
        Top_panel.SetActive(false);
    }


    public void On_click_contact_us()
    {
        Contact_us_panel.SetActive(true);
    }

    public void fragment_claim()
    {
        int a = PlayerPrefs.GetInt(fragment_panel.GetComponent<fragment_panel_script>().skin_name);
        PlayerPrefs.SetInt(fragment_panel.GetComponent<fragment_panel_script>().skin_name, a + 3);
        fragment_panel.SetActive(false);
        //Ads_priority_script.Instance.frag_watch_video();
    }

    public void FrageMent_skin_open()
    {
        int sel = Random.Range(0, 3);
        int a;
        string name_s;
        switch (sel)
        {
            case 0:
                a = Random.Range(0, Setting_API.Instance.product_id_redeem_MLBB.Length);
                name_s = Setting_API.Instance.name_redeem_MLBB[a];

                fragment_panel.GetComponent<fragment_panel_script>().skin_name = name_s;
                fragment_panel.GetComponent<fragment_panel_script>().skin.sprite =  Redeem_panel.GetComponent<Redeem_panel_script>().mlbb_content.transform.GetChild(a).GetComponent<skin_redeem_but_script>().SkinImage.sprite;

                break;
            case 1:
                a = Random.Range(0, Setting_API.Instance.product_id_redeem_FF.Length);
                name_s = Setting_API.Instance.name_redeem_FF[a];
                fragment_panel.GetComponent<fragment_panel_script>().skin_name = name_s;
                fragment_panel.GetComponent<fragment_panel_script>().skin.sprite = Redeem_panel.GetComponent<Redeem_panel_script>().freefire_content.transform.GetChild(a).GetComponent<skin_redeem_but_script>().SkinImage.sprite;
                break;
            case 2:
                a = Random.Range(0, Setting_API.Instance.product_id_redeem_ROB.Length);
                name_s = Setting_API.Instance.name_redeem_ROB[a];
                fragment_panel.GetComponent<fragment_panel_script>().skin_name = name_s;
                fragment_panel.GetComponent<fragment_panel_script>().skin.sprite = Redeem_panel.GetComponent<Redeem_panel_script>().roblox_content.transform.GetChild(a).GetComponent<skin_redeem_but_script>().SkinImage.sprite;
                break;
                //default:
        }


        fragment_panel.SetActive(true);

    }

    public void FrageMent_skin_open(string s_name , Sprite s_image)
    {
        fragment_panel.GetComponent<fragment_panel_script>().skin_name = s_name;
        fragment_panel.GetComponent<fragment_panel_script>().skin.sprite = s_image;
        fragment_panel.SetActive(true);
    }

    public void contact_us()
    {
        AnimationManager.instance.Contact_mail();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Close_click();
        }
    }
}

