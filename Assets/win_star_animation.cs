﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum star_pos { left, mid, right}
public class win_star_animation : MonoBehaviour
{

    public star_pos star_pos_;
    private void OnEnable()
    {
        
        transform.position = new Vector3(transform.position.x,0,transform.position.z);
        //y = 272


        switch (star_pos_)
        {
            case star_pos.left:
                transform.DOLocalMoveY(272f,0.5f);

                break;
            case star_pos.mid:
                transform.DOLocalMoveY(310f, 0.5f);
                break;
            case star_pos.right:
                transform.DOLocalMoveY(272f, 0.5f);
                break;
            default:
                break;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
