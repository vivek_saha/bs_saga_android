﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{

    public GameObject Canvas_singleton;

    public GameObject popup_message;





    protected override void OnApplicationQuitCallback()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnEnableCallback()
    {
        //throw new System.NotImplementedException();
    }



    public void nem_popup_fun(string msg)
    {
        _ShowAndroidToastMessage(msg);
        //GameObject a = Instantiate(popup_message);
        //a.GetComponent<nem_popup_script>().msg.text = msg;
        //a.transform.SetParent(this.transform);
        //a.transform.localScale = Vector3.one;
        //a.transform.position = Canvas_singleton_script.Instance.origin.transform.position;
        //a.transform.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
    }


    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, message, 0);
            toastObject.Call("show");
            //unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            //{
            //}));
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }



}
