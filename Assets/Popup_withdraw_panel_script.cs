﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Popup_withdraw_panel_script : MonoBehaviour
{



    public static Popup_withdraw_panel_script Instance;


    public string p_id;

    public coin_type ct;
    public Text price_value;
    public InputField c_input;


    private void Awake()
    {
        Instance = this;

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void On_submit_claim_click()
    {
        if (c_input.text != null)
        {
            withdraw_skin_dia_Api_script.Instance.SEt_update_data(p_id,c_input.text);
            gameObject.SetActive(false);
            //withdraw_skin_dia_Api_script.Instance.p_id=p_id;
        }
        else
        {
            GameManager.Instance.nem_popup_fun("Please fill Character Id!");
        }
    }
}
