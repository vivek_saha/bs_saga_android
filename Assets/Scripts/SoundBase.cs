using System;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundBase : MonoBehaviour
{
	private void Awake()
	{
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		SoundBase.Instance = this;
	}

	private void Update()
	{
	}

	public static SoundBase Instance;

	public AudioClip click;

	public AudioClip[] combo;

	public AudioClip[] swish;

	public AudioClip bug;

	public AudioClip bugDissapier;

	public AudioClip[] pops;

	public AudioClip boiling;

	public AudioClip hit;

	public AudioClip kreakWheel;

	public AudioClip spark;

	public AudioClip winSound;

	public AudioClip gameOver;

	public AudioClip scoringStar;

	public AudioClip scoring;

	public AudioClip alert;

	public AudioClip aplauds;

	public AudioClip OutOfMoves;

	public AudioClip Boom;

	public AudioClip black_hole;

	public AudioClip GotDailyReward;

	public AudioClip swipeBanner;

	public AudioClip BallFire;

	public AudioClip BallExplode;

	public AudioClip GetStarPlaying;

	public AudioClip MapStar;

	public AudioClip ReceiveCoin;

	public AudioClip ClockTic_Tic;

	public AudioClip TicTicSound;

	public AudioClip Stars;

	public AudioClip helicopter;

	public AudioClip Fireworks;
}
