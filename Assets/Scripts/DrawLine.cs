using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class DrawLine : MonoBehaviour
{
	private void Start()
	{
		this.line = base.GetComponent<LineRenderer>();
		this.GeneratePoints();
		this.GeneratePositionsPoints();
		this.HidePoints();
		DrawLine.waypoints[0] = ball_init_pos.transform.position;
		//Debug.Log("base.transform.position :" + base.transform.position);
		DrawLine.waypoints[1] = base.transform.position + Vector3.up * 5f;
		this.swapped = true;
	}

	private void HidePoints()
	{
		foreach (GameObject gameObject in this.pointers)  

		{
			gameObject.GetComponent<SpriteRenderer>().enabled = false;
		}
		foreach (GameObject gameObject2 in this.pointers2)
		{
			gameObject2.GetComponent<SpriteRenderer>().enabled = false;
		}
	}

	private void GeneratePositionsPoints()
	{
		if (mainscript.Instance.boxCatapult.GetComponent<Grid>().Busy != null)
		{
			this.col = mainscript.Instance.boxCatapult.GetComponent<Grid>().Busy.GetComponent<SpriteRenderer>().sprite.texture.GetPixelBilinear(0.6f, 0.6f);
			this.col.a = 1f;
		}
		this.HidePoints();
		for (int i = 0; i < this.pointers.Length; i++)
		{
			Vector2 normalized = (DrawLine.waypoints[1] - DrawLine.waypoints[0]).normalized;
			float num = (float)i / 1.5f;
			if (num < (DrawLine.waypoints[1] - DrawLine.waypoints[0]).magnitude)
			{
				this.pointers[i].GetComponent<SpriteRenderer>().enabled = true;
				this.pointers[i].transform.position = DrawLine.waypoints[0] + num * normalized;
				this.pointers[i].GetComponent<SpriteRenderer>().color = this.col;
				this.pointers[i].GetComponent<LinePoint>().startPoint = this.pointers[i].transform.position;
				this.pointers[i].GetComponent<LinePoint>().nextPoint = this.pointers[i].transform.position;
				if (i > 0)
				{
					this.pointers[i - 1].GetComponent<LinePoint>().nextPoint = this.pointers[i].transform.position;
				}
			}
		}
		for (int j = 0; j < this.pointers2.Length; j++)
		{
			Vector2 normalized2 = (DrawLine.waypoints[2] - DrawLine.waypoints[1]).normalized;
			float num2 = (float)j / 2f;
			if (num2 < (DrawLine.waypoints[2] - DrawLine.waypoints[1]).magnitude)
			{
				this.pointers2[j].GetComponent<SpriteRenderer>().enabled = true;
				this.pointers2[j].transform.position = DrawLine.waypoints[1] + num2 * normalized2;
				this.pointers2[j].GetComponent<SpriteRenderer>().color = this.col;
				this.pointers2[j].GetComponent<LinePoint>().startPoint = this.pointers2[j].transform.position;
				this.pointers2[j].GetComponent<LinePoint>().nextPoint = this.pointers2[j].transform.position;
				if (j > 0)
				{
					this.pointers2[j - 1].GetComponent<LinePoint>().nextPoint = this.pointers2[j].transform.position;
				}
			}
		}
	}

	private void GeneratePoints()
	{
		for (int i = 0; i < this.pointers.Length; i++)
		{
			this.pointers[i] = UnityEngine.Object.Instantiate<GameObject>(this.pointer, base.transform.position, base.transform.rotation);
			this.pointers[i].transform.parent = base.transform;
		}
		for (int j = 0; j < this.pointers2.Length; j++)
		{
			this.pointers2[j] = UnityEngine.Object.Instantiate<GameObject>(this.pointer, base.transform.position, base.transform.rotation);
			this.pointers2[j].transform.parent = base.transform;
		}
	}

	private void Update()
	{
		this.pointers[0].GetComponent<SpriteRenderer>().enabled = false;
		this.pointers[1].GetComponent<SpriteRenderer>().enabled = false;
		if (Input.GetMouseButton(0))
		{
#if UNITY_EDITOR
			this.MousePosition = Input.mousePosition;
#else
			this.MousePosition = UnityEngine.Input.GetTouch(0).position;
#endif
		}
		if (Input.GetMouseButtonDown(0) && !EventSystem.current.currentSelectedGameObject && GamePlay.Instance.GameStatus == GameState.Playing)
		{
			mainscript.Instance.Layer.SetActive(true);
			base.StopCoroutine("WapingTrue");
			mainscript.Instance.BallChanged = false;

#if UNITY_EDITOR
			DrawLine.BallPrepare = true;
			this.draw = true;
#else


			if (UnityEngine.Input.touchCount == 1)
			{
				DrawLine.BallPrepare = true;
				this.draw = true;
			}
#endif
		}
		else if (Input.GetMouseButtonUp(0))
		{
			this.draw = false;
			base.StartCoroutine("WapingTrue");
		}
		if (this.draw)
		{
			Vector3 vector = Camera.main.ScreenToWorldPoint(this.MousePosition) - Vector3.back * 10f;
			Ray ray = Camera.main.ScreenPointToRay(this.MousePosition);
			if (!mainscript.StopControl)
			{
				vector.z = 0f;
				if (this.lastMousePos == vector)
				{
					this.startAnim = true;
				}
				else
				{
					this.startAnim = false;
				}
				this.lastMousePos = vector;
				this.line.SetPosition(0, base.transform.position);
				DrawLine.waypoints[0] = ball_init_pos.transform.position;
				RaycastHit2D[] array = Physics2D.LinecastAll(DrawLine.waypoints[0], DrawLine.waypoints[0] + ((Vector2)vector - DrawLine.waypoints[0]).normalized * 10f);
				foreach (RaycastHit2D raycastHit2D in array)
				{
					Vector2 point = raycastHit2D.point;
					this.line.SetPosition(1, point);
					this.addAngle = 180f;
					if (DrawLine.waypoints[1].x < 0f)
					{
						this.addAngle = 0f;
					}
					if (raycastHit2D.collider.gameObject.layer == LayerMask.NameToLayer("Border") && raycastHit2D.collider.gameObject.name != "GameOverBorder" && raycastHit2D.collider.gameObject.name != "borderForRoundedLevels")
					{
						UnityEngine.Debug.DrawLine(DrawLine.waypoints[0], DrawLine.waypoints[1], Color.red);
						UnityEngine.Debug.DrawLine(DrawLine.waypoints[0], vector, Color.blue);
						UnityEngine.Debug.DrawRay(DrawLine.waypoints[0], DrawLine.waypoints[1] - DrawLine.waypoints[0], Color.green);
						DrawLine.waypoints[1] = point;
						DrawLine.waypoints[2] = point;
						this.line.SetPosition(1, vector);
						DrawLine.waypoints[1] = point;
						float num = Vector2.Angle(DrawLine.waypoints[0] - DrawLine.waypoints[1], point - Vector2.up * 100f - point);
						if (num > 60f)
						{
							for (int j = 0; j < GameObject.Find("Line").transform.childCount; j++)
							{
								GameObject.Find("Line").transform.GetChild(j).GetComponent<SpriteRenderer>().enabled = false;
							}
						}
						if (DrawLine.waypoints[1].x > 0f)
						{
							num = Vector2.Angle(DrawLine.waypoints[0] - DrawLine.waypoints[1], point - (point - Vector2.up * 100f));
						}
						DrawLine.waypoints[2] = Quaternion.AngleAxis(num + this.addAngle, Vector3.back) * (point - (point - Vector2.up * 100f));
						Vector2 vector2 = DrawLine.waypoints[2] - DrawLine.waypoints[1];
						float num2 = num + this.addAngle;
						float num3;
						if (num2 > 180f)
						{
							num3 = -(360f - num2);
							if (num3 <= -60f)
							{
								num3 = -60f;
							}
						}
						else
						{
							num3 = num2;
							if (num3 >= 60f)
							{
								num3 = 60f;
							}
						}
						//GameObject.Find("paotong").transform.eulerAngles = new Vector3(0f, 0f, num3);
						vector2 = vector2.normalized;
						this.line.SetPosition(2, DrawLine.waypoints[2]);
						break;
					}
					if (raycastHit2D.collider.gameObject.layer == LayerMask.NameToLayer("Ball"))
					{
						UnityEngine.Debug.DrawLine(DrawLine.waypoints[0], DrawLine.waypoints[1], Color.red);
						UnityEngine.Debug.DrawLine(DrawLine.waypoints[0], vector, Color.blue);
						UnityEngine.Debug.DrawRay(DrawLine.waypoints[0], DrawLine.waypoints[1] - DrawLine.waypoints[0], Color.green);
						this.line.SetPosition(1, point);
						DrawLine.waypoints[1] = point;
						float num4 = Vector2.Angle(DrawLine.waypoints[0] - DrawLine.waypoints[1], point - Vector2.up * 100f - point);
						if (DrawLine.waypoints[1].x > 0f)
						{
							num4 = Vector2.Angle(DrawLine.waypoints[0] - DrawLine.waypoints[1], point - (point - Vector2.up * 100f));
						}
						DrawLine.waypoints[2] = point;
						Vector2 a = DrawLine.waypoints[2] - DrawLine.waypoints[1];
						float num5 = num4 + this.addAngle;
						float num6;
						if (num5 > 180f)
						{
							num6 = -(360f - num5);
							if (num6 <= -30f)
							{
								num6 = -30f;
							}
						}
						else
						{
							num6 = num5;
							if (num6 > 100f)
							{
								num6 = 180f - num6;
							}
						}
						//GameObject.Find("paotong").transform.localRotation = Quaternion.Euler(0f, 0f, num6);
						a = a.normalized;
						this.line.SetPosition(2, DrawLine.waypoints[1] + 0.1f * a);
						break;
					}
					DrawLine.waypoints[1] = DrawLine.waypoints[0] + ((Vector2)vector - DrawLine.waypoints[0]).normalized * 10f;
					DrawLine.waypoints[2] = DrawLine.waypoints[0] + ((Vector2)vector - DrawLine.waypoints[0]).normalized * 10f;
				}
				if (!this.startAnim)
				{
					this.GeneratePositionsPoints();
				}
			}
		}
		else if (!this.draw)
		{
			this.HidePoints();
		}
	}

	private IEnumerator WapingTrue()
	{
		yield return new WaitForSeconds(0.6f);
		mainscript.Instance.BallChanged = true;
		yield break;
	}

	private LineRenderer line;

	private bool draw;

	private Color col;

	public static bool BallPrepare = false;

	public static Vector2[] waypoints = new Vector2[3];

	public float addAngle = 90f;

	public GameObject pointer;

	private GameObject[] pointers = new GameObject[15];

	private GameObject[] pointers2 = new GameObject[3];

	private Vector3 lastMousePos;

	private bool startAnim;

	public static bool BallFire = false;

	private Vector3 MousePosition;

	private bool swapped;

	public GameObject ball_init_pos;
}
