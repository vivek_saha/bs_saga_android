using System;
using UnityEngine;
using UnityEngine.UI;

public class Offlineleadboard : MonoBehaviour
{
	private void OnEnable()
	{
		this.label = base.transform.Find("Slot").Find("Score").GetComponent<Text>();
		this.label.text = "Best Score: " + PlayerPrefs.GetInt("Score" + PlayerPrefs.GetInt("OpenLevel"));
	}

	private void OnDisable()
	{
		this.label.text = string.Empty;
	}

	private Text label;
}
