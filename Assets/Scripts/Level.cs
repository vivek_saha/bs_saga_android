using System;
using System.Collections;
using InitScriptName;
using UnityEngine;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
	public static Level instance { get; set; }


	int ads_counter = 0;

    private void OnEnable()
    {
		//this.label.GetComponent<MeshRenderer>().sortingOrder = 1;
	}
    private void Start()
	{
		//PlayerPrefs.SetInt("OpenLevel", 20);
		Level.instance = this;
		//Debug.Log("start_numbber" + this.number);
		//this.label.GetComponent<TextMesh>().text = this.number.ToString("00");
		//this.label.GetComponent<MeshRenderer>().sortingOrder = 1;
		//alllvlunlocked

		if (PlayerPrefs.GetInt("Score" + (this.number - 1)) > 0 || this.number == 1)
		{
			if(lockimage!= null)
			this.lockimage.gameObject.SetActive(false);
		}
		int @int = PlayerPrefs.GetInt(string.Format("Level.{0:000}.StarsCount", this.number), 0);
		if (@int > 0)
		{
			for (int i = 1; i <= 3; i++)
			{
				if (@int >= i)
				{
					//base.transform.Find("Star" + i).gameObject.SetActive(true);
				}
				else
				{
					//base.transform.Find("Star" + i).gameObject.SetActive(false);
				}
			}
		}
		else
		{
			//base.transform.Find("Star1").gameObject.SetActive(false);
			//base.transform.Find("Star2").gameObject.SetActive(false);
			//base.transform.Find("Star3").gameObject.SetActive(false);
			//ProgressBarScript.Instance.star1.gameObject.SetActive(false);
			//ProgressBarScript.Instance.star2.gameObject.SetActive(false);
			//ProgressBarScript.Instance.star3.gameObject.SetActive(false);
		}
		if (PlayerPrefs.GetString("Scene") == "game" && PlayerPrefs.GetString("LevelPass") == "Yes" && this.number == PlayerPrefs.GetInt("OpenLevel"))
		{
			//MonoBehaviour.print(string.Concat(new object[]
			//{
			//	"====number=====>",
			//	this.number,
			//	"-----PlayerPrefs.GetInt(",
			//	PlayerPrefs.GetInt("OpenLevel")
			//}));
			//base.StartCoroutine(this.StarsAnim(base.gameObject));
			//base.transform.Find("Star1").gameObject.SetActive(false);
			//base.transform.Find("Star2").gameObject.SetActive(false);
			//base.transform.Find("Star3").gameObject.SetActive(false);

			//ProgressBarScript.Instance.star1.gameObject.SetActive(false);
			//ProgressBarScript.Instance.star2.gameObject.SetActive(false);
			//ProgressBarScript.Instance.star3.gameObject.SetActive(false);
		}
		StartLevel();
	}

	private void Update()
	{
	}

	public void but_click_play()
    {

		//charater_anim.GetComponent<Animator>().Play("Play_hit_animation");

		if (ads_counter >= Ads_API.Instance.ads_click)
		{
			ads_counter = 0;
			Ads_priority_script.Instance.Show_interrestial();
		}
		else
        {
			ads_counter++;
        }
        InitScript.Instance.OnLevelClicked(PlayerPrefs.GetInt("OpenLevel", 1));
		//InitScript.Instance.OnLevelClicked(this.number);
	}

	public void StartLevel()
	{
		////if()
		//if (!this.lockimage.activeSelf)
		//{
		//	InitScript.Instance.OnLevelClicked(this.number);
		//}
	}

	public void StartLevelMap()
	{

		this.number = PlayerPrefs.GetInt("MaxLevel", 0) + 1;
		//Debug.Log("level here "+this.number);
		InitScript.Instance.OnLevelClicked(this.number);
	}

	private IEnumerator StarsAnim(GameObject game)
	{
		yield return new WaitForSeconds(1f);

		Debug.Log("gotcha");
		//Time.timeScale = 0;
        for (int i = 0; i < mainscript.Instance.stars; i++)
		{
			SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.winSound);
			GameObject.Find("Stars").transform.GetChild(i).gameObject.SetActive(true);
			GameObject.Find("Stars").transform.GetChild(i).GetComponent<Animator>().enabled = true;
			GameObject.Find("Stars").transform.GetChild(i).GetComponent<Animator>().Play("RiseUpStarMap");
		}
		yield return new WaitForSeconds(1f);
		for (int j = 0; j < mainscript.Instance.stars; j++)
		{
			SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.winSound);
			GameObject.Find("Stars").transform.GetChild(j).transform.GetChild(0).gameObject.SetActive(true);
			GameObject.Find("Coins").transform.GetChild(j).gameObject.SetActive(true);
		}
		yield return new WaitForSeconds(0.5f);
		CanvasManager.instance.Open_double_coin_panel(this.gameObject);
		yield return new WaitForSeconds(0.5f);
		for (int k = 1; k <= mainscript.Instance.stars; k++)
		{
			GameObject.Find("Coin" + k).transform.parent = GameObject.Find("Canvas").transform;
			GameObject.Find("Coin" + k).transform.GetComponent<SmoothMovementCoin>().Double_coin = double_coin;
			GameObject.Find("Coin" + k).transform.gameObject.AddComponent<RectTransform>();
			GameObject.Find("Coin" + k).transform.gameObject.AddComponent<Image>();
			GameObject.Find("Coin" + k).transform.gameObject.GetComponent<Image>().sprite = GameObject.Find("Coin" + k).transform.gameObject.GetComponent<SpriteRenderer>().sprite;
			GameObject.Find("Coin" + k).transform.GetComponent<SpriteRenderer>().enabled = false;
			GameObject.Find("Coin" + k).transform.GetComponent<SmoothMovementCoin>().enabled = true;
			//if(double_coin)
		}
        yield return new WaitForSeconds(2f);
		for (int l = 0; l < mainscript.Instance.stars; l++)
		{
			GameObject.Find("Stars").transform.GetChild(l).GetComponent<Animator>().Play("FirstStarAnim");
			GameObject.Find("Stars").transform.GetChild(l).GetComponent<SmoothMovementStar>().enabled = true;
			GameObject.Find("Stars").transform.GetChild(l).GetComponent<SmoothMovementStar>().targetPos = game.transform.Find("Star" + (l + 1)).transform.position;
		}
		yield return new WaitForSeconds(3f);
		for (int m = 0; m < mainscript.Instance.stars; m++)
		{
			base.transform.Find("Star" + (m + 1)).gameObject.SetActive(true);
			GameObject.Find("Stars").transform.GetChild(m).gameObject.SetActive(false);
		}
        Update_API.Instance.type = "Level_finish";
        Update_API.Instance.Update_data();
		double_coin = false;
		yield break;
	}

	public int number;

	public GameObject label;

	public GameObject lockimage;

	public GameObject charater_anim;

	public bool double_coin = false;
}
