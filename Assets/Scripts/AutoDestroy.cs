using System;
using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
	private void OnEnable()
	{
		base.Invoke("Hide", 3f);
	}

	private void Hide()
	{
		base.gameObject.SetActive(false);
	}

	private void Update()
	{
	}
}
