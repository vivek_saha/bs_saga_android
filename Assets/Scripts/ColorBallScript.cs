using System;
using UnityEngine;

public class ColorBallScript : MonoBehaviour
{
    private void Start()
    {

    }
    public void SetColor(BallColor color)
    {
        this.mainColor = color;



        foreach (Sprite sprite in this.sprites)
        {
            if (sprite.name == "ball_" + color)
            {
                base.GetComponent<SpriteRenderer>().sprite = sprite;
                this.SetSettings(color);
                base.gameObject.tag = string.Empty + color;
            }
        }

    }

    public void SetColorBaby(BallColor color)
    {
        this.mainColor = color;
        foreach (Sprite sprite in this.sprites)
        {
            if (sprite.name == "ball_" + color + "baby")
            {
                base.GetComponent<SpriteRenderer>().sprite = sprite;
                base.gameObject.tag = string.Empty + color + "baby";
            }
        }
    }
    public void SetColorCoin(BallColor color)
    {
        this.mainColor = color;
        foreach (Sprite sprite in this.sprites)
        {
            if (sprite.name == "ball_" + color)
            {
                base.GetComponent<SpriteRenderer>().sprite = sprite;


                string value = color.ToString().Substring(0, color.ToString().Length - 4);
                base.gameObject.tag = string.Empty + value;
            }
        }
    }
    public void SetColorDiamond(BallColor color)
    {
        //Debug.Log(color);
        this.mainColor = color;
        foreach (Sprite sprite in this.sprites)
        {
            if (sprite.name == "ball_" + color)
            {
                base.GetComponent<SpriteRenderer>().sprite = sprite;

                string value = color.ToString().Substring(0, color.ToString().Length - 7);

                base.gameObject.tag = string.Empty + value;
            }
        }
    }

    public void SetColorFragment(BallColor color)
    {
        //Debug.Log(color);
        this.mainColor = color;
        foreach (Sprite sprite in this.sprites)
        {
            if (sprite.name == "ball_" + color)
            {
                base.GetComponent<SpriteRenderer>().sprite = sprite;

                string value = color.ToString().Substring(0, color.ToString().Length - 8);

                base.gameObject.tag = string.Empty + value;
            }
        }
    }

    private void SetSettings(BallColor color)
    {
        if (LevelData.mode == ModeGame.Rounded)
        {
        }
    }

    public void SetColor(int color)
    {
        this.mainColor = (BallColor)color;
        base.GetComponent<SpriteRenderer>().sprite = this.sprites[color];
    }

    public void ChangeRandomColor()
    {
        mainscript.Instance.GetColorsInGame();
        this.SetColor(mainscript.colorsDict[UnityEngine.Random.Range(0, mainscript.colorsDict.Count)]);
        base.GetComponent<Animation>().Stop();
    }

    private void Update()
    {
        if (base.transform.position.y <= -16f && base.transform.parent == null)
        {
            UnityEngine.Object.Destroy(base.gameObject);
        }
    }

    public Sprite[] sprites;

    public BallColor mainColor;

    //public Sprite fragment_sprite;

    public ball_type balltype;
}
