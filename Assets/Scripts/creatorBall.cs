using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ball_type { normal, coin, diamond, fragment }
public class creatorBall : MonoBehaviour
{
    private void Start()
    {
        creatorBall.Instance = this;
        this.ball = this.ball_hd;
        this.bug = this.bug_hd;
        this.thePrefab.transform.localScale = new Vector3(0.67f, 0.58f, 1f);
        this.Meshes = GameObject.Find("-Ball");
        this.LoadLevel();
        if (LevelData.mode == ModeGame.Vertical || LevelData.mode == ModeGame.Animals)
        {
            this.MoveLevelUp();
        }
        else
        {
            GameObject.Find("TopBorder").transform.parent = null;
            GameObject.Find("TopBorder").GetComponent<SpriteRenderer>().enabled = false;
            GameObject gameObject = GameObject.Find("-Meshes");
            gameObject.transform.position += Vector3.up * 2f;
            LockLevelRounded lockLevelRounded = gameObject.AddComponent<LockLevelRounded>();
            GamePlay.Instance.GameStatus = GameState.PreTutorial;
        }
        this.createMesh();
        this.LoadMap(LevelData.map);
        Camera.main.GetComponent<mainscript>().connectNearBallsGlobal();
        base.StartCoroutine(this.getBallsForMesh());
        this.ShowBugs();
    }

    public void LoadLevel()
    {
        mainscript.Instance.currentLevel = PlayerPrefs.GetInt("OpenLevel");
        if (mainscript.Instance.currentLevel == 0)
        {
            mainscript.Instance.currentLevel = 1;
        }
        this.LoadDataFromLocal(mainscript.Instance.currentLevel);
    }

    public bool LoadDataFromLocal(int currentLevel)
    {
        //MonoBehaviour.print("-------======Current Levels---====>" + currentLevel);
        TextAsset textAsset = Resources.Load("Levels/" + currentLevel) as TextAsset;
        if (textAsset == null)
        {
            textAsset = (Resources.Load("Levels/" + currentLevel) as TextAsset);
        }
        this.ProcessGameDataFromString(textAsset.text);
        return true;
    }

    private void ProcessGameDataFromString(string mapText)
    {
        string[] array = mapText.Split(new string[]
        {
            "\n"
        }, StringSplitOptions.RemoveEmptyEntries);
        //MonoBehaviour.print("lines------>" + array.Length);
        LevelData.colorsDict.Clear();
        int num = 0;
        int num2 = 0;
        foreach (string text in array)
        {
            if (text.StartsWith("MODE "))
            {
                string s = text.Replace("MODE", string.Empty).Trim();
                LevelData.mode = (ModeGame)int.Parse(s);
            }
            else if (text.StartsWith("SIZE "))
            {
                string text2 = text.Replace("SIZE", string.Empty).Trim();
                string[] array3 = text2.Split(new string[]
                {
                    "/"
                }, StringSplitOptions.RemoveEmptyEntries);
                this.maxCols = int.Parse(array3[0]);
                this.maxRows = int.Parse(array3[1]);
            }
            else if (text.StartsWith("LIMIT "))
            {
                string text3 = text.Replace("LIMIT", string.Empty).Trim();
                string[] array4 = text3.Split(new string[]
                {
                    "/"
                }, StringSplitOptions.RemoveEmptyEntries);
                this.limitType = (LIMIT)int.Parse(array4[0]);
                LevelData.LimitAmount = (float)int.Parse(array4[1]);
            }
            else if (text.StartsWith("COLOR LIMIT "))
            {
                string s2 = text.Replace("COLOR LIMIT", string.Empty).Trim();
                LevelData.colors = int.Parse(s2);
            }
            else if (text.StartsWith("STARS "))
            {
                string text4 = text.Replace("STARS", string.Empty).Trim();
                string[] array5 = text4.Split(new string[]
                {
                    "/"
                }, StringSplitOptions.RemoveEmptyEntries);
                LevelData.star1 = int.Parse(array5[0]);
                LevelData.star2 = int.Parse(array5[1]);
                LevelData.star3 = int.Parse(array5[2]);
            }
            else if (text.StartsWith("CollectNo "))
            {
                string s3 = text.Replace("CollectNo", string.Empty).Trim();
                mainscript.Instance.TotalTargets = int.Parse(s3);
            }
            else
            {
                string[] array6 = text.Split(new string[]
                {
                    " "
                }, StringSplitOptions.RemoveEmptyEntries);
                //MonoBehaviour.print("st------>" + array6.Length);
                for (int j = 0; j < array6.Length; j++)
                {
                    //MonoBehaviour.print("st------>" + array6[j]);
                    int num3 = int.Parse(array6[j].ToString());
                    if (!LevelData.colorsDict.ContainsValue((BallColor)num3) && num3 > 0 && num3 < 9)
                    {
                        LevelData.colorsDict.Add(num2, (BallColor)num3);
                        num2++;
                    }
                    LevelData.map[num * this.maxCols + j] = int.Parse(array6[j].ToString());
                }
                num++;
            }
        }
        if (LevelData.colorsDict.Count == 0)
        {
            LevelData.colorsDict.Add(0, BallColor.orange);
            LevelData.colorsDict.Add(1, BallColor.red);
            List<BallColor> list_2 = new List<BallColor>();
            list_2.Add(BallColor.blue);
            list_2.Add(BallColor.green);
            list_2.Add(BallColor.violet);
            list_2.Add(BallColor.pink);
            list_2.Add(BallColor.skyblue);
            list_2.Add(BallColor.yellow);
            for (int k = 0; k < LevelData.colors - 2; k++)
            {
                BallColor value = BallColor.orange;
                //while (LevelData.colorsDict.ContainsValue(value))
                //{
                //    value = list[UnityEngine.Random.RandomRange(0, list.Count)];
                //}
                LevelData.colorsDict.Add(2 + k, list_2[k]);
            }
        }
    }

    public void LoadMap(int[] pMap)
    {
        Debug.Log(LevelData.mode);
        this.map = pMap;
        for (int i = 0; i < creatorBall.rows; i++)
        {
            for (int j = 0; j < creatorBall.columns; j++)
            {
                int num = this.map[i * creatorBall.columns + j];
                if (num > 0)
                {
                    int row = i;
                    if (LevelData.mode == ModeGame.Rounded)
                    {
                        row = i + 4;
                    }
                    this.createBall(this.GetSquare(row, j).transform.position, (BallColor)num, false, i, num);
                    //MonoBehaviour.print("11111111----->");
                }
                else if (num != 0 || LevelData.mode != ModeGame.Vertical || i == 0)
                {
                }
            }
        }
    }

    private void MoveLevelUp()
    {
        base.StartCoroutine(this.MoveUpDownCor(false));
    }

    private IEnumerator MoveUpDownCor(bool inGameCheck = false)
    {
        yield return new WaitForSeconds(0.1f);
        if (!inGameCheck)
        {
            GamePlay.Instance.GameStatus = GameState.BlockedGame;
        }
        bool up = false;
        List<float> table = new List<float>();
        float lineY = -1.3f;
        Transform bubbles = GameObject.Find("-Ball").transform;
        int i = 0;
        IEnumerator enumerator = bubbles.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                object obj = enumerator.Current;
                Transform transform = (Transform)obj;
                if (!inGameCheck)
                {
                    if (transform.position.y < lineY)
                    {
                        table.Add(transform.position.y);
                    }
                }
                else if (!transform.GetComponent<ball>().Destroyed)
                {
                    if (transform.position.y > lineY && mainscript.Instance.TopBorder.transform.position.y > 5f)
                    {
                        table.Add(transform.position.y);
                    }
                    else if (transform.position.y < lineY + 1f)
                    {
                        table.Add(transform.position.y);
                        up = true;
                    }
                }
                i++;
            }
        }
        finally
        {
            IDisposable disposable;
            if ((disposable = (enumerator as IDisposable)) != null)
            {
                disposable.Dispose();
            }
        }
        if (table.Count > 0)
        {
            if (up)
            {
                this.AddMesh();
            }
            float targetY = 0f;
            table.Sort();
            if (!inGameCheck)
            {
                targetY = lineY - table[0] + 2.5f;
            }
            else
            {
                targetY = lineY - table[0] + 1.5f;
            }
            GameObject Meshes = GameObject.Find("-Meshes");
            Vector3 targetPos = Meshes.transform.position + Vector3.up * targetY;
            float startTime = Time.time;
            Vector3 startPos = Meshes.transform.position;
            float speed = 0.5f;
            float distCovered = 0f;
            while (distCovered < 1f)
            {
                speed += Time.deltaTime / 1.5f;
                distCovered = (Time.time - startTime) / speed;
                Meshes.transform.position = Vector3.Lerp(startPos, targetPos, distCovered);
                yield return new WaitForEndOfFrame();
                if (startPos.y > targetPos.y && mainscript.Instance.TopBorder.transform.position.y <= 5f && inGameCheck)
                {
                    break;
                }
            }
        }
        if (GamePlay.Instance.GameStatus == GameState.BlockedGame)
        {
            GamePlay.Instance.GameStatus = GameState.PreTutorial;
        }
        else if (GamePlay.Instance.GameStatus != GameState.GameOver && GamePlay.Instance.GameStatus != GameState.Win)
        {
            GamePlay.Instance.GameStatus = GameState.Playing;
        }
        yield break;
    }

    public void MoveLevelDown()
    {
        base.StartCoroutine(this.MoveUpDownCor(true));
    }

    private bool BubbleBelowLine()
    {
        throw new NotImplementedException();
    }

    private void ShowBugs()
    {
        int num = 1;
        for (int i = 0; i < 2; i++)
        {
            num *= -1;
            //this.CreateBug(new Vector3((float)(10 * num), -3f, 0f), 1);
        }
    }

    public void CreateBug(Vector3 pos, int value = 1)
    {
        Transform transform = GameObject.Find("Spiders").transform;
        List<Bug> list = new List<Bug>();
        IEnumerator enumerator = transform.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                object obj = enumerator.Current;
                Transform transform2 = (Transform)obj;
                if (transform2.childCount > 0)
                {
                    list.Add(transform2.GetChild(0).GetComponent<Bug>());
                }
            }
        }
        finally
        {
            IDisposable disposable;
            if ((disposable = (enumerator as IDisposable)) != null)
            {
                disposable.Dispose();
            }
        }
        if (list.Count < 6)
        {
            UnityEngine.Object.Instantiate<GameObject>(this.bug, pos, Quaternion.identity);
        }
        else
        {
            list.Clear();
            IEnumerator enumerator2 = transform.GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    object obj2 = enumerator2.Current;
                    Transform transform3 = (Transform)obj2;
                    if (transform3.childCount > 0 && transform3.GetChild(0).GetComponent<Bug>().color == 0)
                    {
                        list.Add(transform3.GetChild(0).GetComponent<Bug>());
                    }
                }
            }
            finally
            {
                IDisposable disposable2;
                if ((disposable2 = (enumerator2 as IDisposable)) != null)
                {
                    disposable2.Dispose();
                }
            }
            if (list.Count > 0)
            {
                list[UnityEngine.Random.Range(0, list.Count)].ChangeColor(1);
            }
        }
    }

    private void Update()
    {
    }

    private IEnumerator getBallsForMesh()
    {
        GameObject[] meshes = GameObject.FindGameObjectsWithTag("Mesh");
        foreach (GameObject gameObject in meshes)
        {
            Collider2D[] array2 = Physics2D.OverlapCircleAll(gameObject.transform.position, 0.2f, 512);
            foreach (Collider2D collider2D in array2)
            {
                gameObject.GetComponent<Grid>().Busy = collider2D.gameObject;
            }
        }
        yield return new WaitForSeconds(0.5f);
        yield break;
    }

    public void EnableGridColliders()
    {
        foreach (GameObject gameObject in this.squares)
        {
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
        }
    }

    public void OffGridColliders()
    {
        foreach (GameObject gameObject in this.squares)
        {
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    public void createRow(int j)
    {
        GameObject gameObject = GameObject.Find("Creator");
        for (int i = 0; i < creatorBall.columns; i++)
        {
            float num;
            if (j % 2 == 0)
            {
                num = 0f;
            }
            else
            {
                num = this.offsetStep;
            }
            Vector3 vec = new Vector3(base.transform.position.x + (float)i * this.thePrefab.transform.localScale.x + num, base.transform.position.y - (float)j * this.thePrefab.transform.localScale.y, base.transform.position.z);
            MonoBehaviour.print("222222222----->");
            this.createBall(vec, BallColor.random, false, 1, 0);
        }
    }

    public GameObject createBall(Vector3 vec, BallColor color = BallColor.random, bool newball = false, int row = 1, int Map = 0)
    {
        //Debug.Log("in...createBall..color:" + color);


        //if (color == BallColor.bluebaby)
        //      {
        //      }
        List<BallColor> list = new List<BallColor>();
        for (int i = 1; i < Enum.GetValues(typeof(BallColor)).Length; i++)
        {
            list.Add((BallColor)i);
        }
        GameObject gameObject;
        if (color.ToString().Contains("coin"))
        {
            gameObject = UnityEngine.Object.Instantiate<GameObject>(this.Ball_coin, base.transform.position, base.transform.rotation);
            gameObject.GetComponent<ColorBallScript>().balltype = ball_type.coin;
            gameObject.GetComponent<ColorBallScript>().SetColorCoin(color);
        }
        else if (color.ToString().Contains("diamond"))
        {
            gameObject = UnityEngine.Object.Instantiate<GameObject>(this.Ball_diamond, base.transform.position, base.transform.rotation);
            gameObject.GetComponent<ColorBallScript>().balltype = ball_type.diamond;
            gameObject.GetComponent<ColorBallScript>().SetColorDiamond(color);
        }
        else if (color.ToString().Contains("fragment"))
        {
            gameObject = UnityEngine.Object.Instantiate<GameObject>(this.Ball_fragment, base.transform.position, base.transform.rotation);
            gameObject.GetComponent<ColorBallScript>().balltype = ball_type.fragment;
            gameObject.GetComponent<ColorBallScript>().SetColorFragment(color);
        }
        else
        {
            gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ball, base.transform.position, base.transform.rotation);
            gameObject.GetComponent<ColorBallScript>().SetColor(color);
        }




        if (color == BallColor.random)
        {
            color = LevelData.colorsDict[UnityEngine.Random.Range(0, LevelData.colorsDict.Count)];
            gameObject.GetComponent<ColorBallScript>().SetColor(color);
        }
        else if (color == BallColor.randomcoin)
        {
            color = LevelData.colorsDict[UnityEngine.Random.Range(0, LevelData.colorsDict.Count)];
            gameObject.GetComponent<ColorBallScript>().SetColorCoin(color);
        }
        else if (color == BallColor.randomdiamond)
        {
            color = LevelData.colorsDict[UnityEngine.Random.Range(0, LevelData.colorsDict.Count)];
            gameObject.GetComponent<ColorBallScript>().SetColorDiamond(color);
        }




        if (newball && mainscript.colorsDict.Count > 0)
        {
            if (GamePlay.Instance.GameStatus == GameState.Playing)
            {
                mainscript.Instance.GetColorsInGame();
                color = mainscript.colorsDict[UnityEngine.Random.Range(0, mainscript.colorsDict.Count)];
                gameObject.GetComponent<ColorBallScript>().SetColor(color);
            }
            else
            {
                color = LevelData.colorsDict[UnityEngine.Random.Range(0, LevelData.colorsDict.Count)];
                gameObject.GetComponent<ColorBallScript>().SetColor(color);
            }
        }
        gameObject.transform.position = new Vector3(vec.x, vec.y, this.ball.transform.position.z);
        gameObject.transform.parent = this.Meshes.transform;
        //if (color.ToString().Contains("coin"))
        //{
        //    //ballType = ball_type.coin;
        //    string text = color.ToString();
        //    text = text.Remove(text.Length - 4);
        //    gameObject.tag = string.Empty + text;
        //}
        //else
        //{
        //    gameObject.tag = string.Empty + color;
        //}
        GameObject[] array = UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) as GameObject[];
        gameObject.name += array.Length.ToString();
        if (newball)
        {
            if (GamePlay.Instance.GameStatus == GameState.Win)
            {
                gameObject.name = "ball";
            }
            gameObject.gameObject.layer = 17;
            gameObject.transform.parent = Camera.main.transform;
            Rigidbody2D rigidbody2D = gameObject.AddComponent<Rigidbody2D>();
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
            rigidbody2D.gravityScale = 0f;
            if (GamePlay.Instance.GameStatus == GameState.Playing)
            {
                gameObject.GetComponent<Animation>().Play();
            }
        }
        else
        {
            gameObject.GetComponent<ball>().enabled = false;
            if (LevelData.mode == ModeGame.Vertical && row == 0)
            {
                gameObject.GetComponent<ball>().isTarget = true;
            }
            gameObject.GetComponent<BoxCollider2D>().offset = Vector2.zero;
            gameObject.GetComponent<BoxCollider2D>().size = new Vector2(0.5f, 0.5f);
        }
        return gameObject.gameObject;
    }

    private void CreateEmptyBall(Vector3 vec)
    {
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ball, base.transform.position, base.transform.rotation);
        gameObject.transform.position = new Vector3(vec.x, vec.y, this.ball.transform.position.z);
        gameObject.GetComponent<ColorBallScript>().SetColor(11);
        gameObject.transform.parent = this.Meshes.transform;
        gameObject.tag = "empty";
        gameObject.GetComponent<ball>().enabled = false;
        gameObject.gameObject.layer = 9;
        gameObject.GetComponent<Animation>().Play("cat_idle");
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 20;
        gameObject.GetComponent<BoxCollider2D>().offset = Vector2.zero;
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(0.5f, 0.5f);
    }

    private int setColorFrame(string sTag)
    {
        int result = 0;
        if (sTag == "Orange")
        {
            result = 7;
        }
        else if (sTag == "Red")
        {
            result = 3;
        }
        else if (sTag == "Yellow")
        {
            result = 1;
        }
        else if (sTag == "Rainbow")
        {
            result = 4;
        }
        else if (sTag == "Pearl")
        {
            result = 6;
        }
        else if (sTag == "Blue")
        {
            result = 11;
        }
        else if (sTag == "DarkBlue")
        {
            result = 8;
        }
        else if (sTag == "Green")
        {
            result = 10;
        }
        else if (sTag == "Pink")
        {
            result = 5;
        }
        else if (sTag == "Violet")
        {
            result = 2;
        }
        else if (sTag == "Brown")
        {
            result = 9;
        }
        else if (sTag == "Gray")
        {
            result = 12;
        }
        return result;
    }

    private int setColorFrameBug(string sTag)
    {
        int result = 0;
        if (sTag == "Orange")
        {
            result = 5;
        }
        else if (sTag == "Red")
        {
            result = 3;
        }
        else if (sTag == "Yellow")
        {
            result = 1;
        }
        else if (sTag == "Rainbow")
        {
            result = 4;
        }
        else if (sTag == "Pearl")
        {
            result = 10;
        }
        else if (sTag == "Blue")
        {
            result = 10;
        }
        else if (sTag == "DarkBlue")
        {
            result = 8;
        }
        else if (sTag == "Green")
        {
            result = 7;
        }
        else if (sTag == "Pink")
        {
            result = 4;
        }
        else if (sTag == "Violet")
        {
            result = 2;
        }
        else if (sTag == "Brown")
        {
            result = 9;
        }
        else if (sTag == "Gray")
        {
            result = 6;
        }
        return result;
    }

    public string getRandomColorTag()
    {
        string result = string.Empty;
        int num;
        if (mainscript.stage < 6)
        {
            num = UnityEngine.Random.Range(0, 4 + mainscript.stage - 1);
        }
        else
        {
            num = UnityEngine.Random.Range(0, 10);
        }
        if (num == 0)
        {
            result = "Orange";
        }
        else if (num == 1)
        {
            result = "Red";
        }
        else if (num == 2)
        {
            result = "Yellow";
        }
        else if (num == 3)
        {
            result = "Rainbow";
        }
        else if (num == 4)
        {
            result = "Blue";
        }
        else if (num == 5)
        {
            result = "Green";
        }
        else if (num == 6)
        {
            result = "Pink";
        }
        else if (num == 7)
        {
            result = "Violet";
        }
        else if (num == 8)
        {
            result = "Brown";
        }
        else if (num == 9)
        {
            result = "Gray";
        }
        return result;
    }

    public void createMesh()
    {
        GameObject gameObject = GameObject.Find("-Meshes");
        for (int i = 0; i < creatorBall.rows + 1; i++)
        {
            for (int j = 0; j < creatorBall.columns; j++)
            {
                float num;
                if (i % 2 == 0)
                {
                    num = 0f;
                }
                else
                {
                    num = this.offsetStep;
                }
                GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.thePrefab, base.transform.position, base.transform.rotation);
                Vector3 localPosition = new Vector3(base.transform.position.x + (float)j * gameObject2.transform.localScale.x + num, base.transform.position.y - (float)i * gameObject2.transform.localScale.y, base.transform.position.z);
                //Debug.Log("base : : " + base.name);
                //Debug.Log("gameObject2.transform.localScale.x : : " + gameObject2.transform.localScale.x);
                //Debug.Log("num : : " + num);
                //Debug.Log("localPosition : : " + localPosition);
                gameObject2.transform.parent = gameObject.transform;
                gameObject2.transform.localPosition = localPosition;
                GameObject[] array = GameObject.FindGameObjectsWithTag("Mesh");
                gameObject2.name += array.Length.ToString();
                gameObject2.GetComponent<Grid>().offset = num;
                this.squares.Add(gameObject2);
                this.lastRow = i;
            }
        }
        creatorBall.Instance.OffGridColliders();
    }

    public void AddMesh()
    {
        GameObject gameObject = GameObject.Find("-Meshes");
        int num = this.lastRow + 1;
        for (int i = 0; i < creatorBall.columns; i++)
        {
            float num2;
            if (num % 2 == 0)
            {
                num2 = 0f;
            }
            else
            {
                num2 = this.offsetStep;
            }
            GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.thePrefab, base.transform.position, base.transform.rotation);
            Vector3 position = new Vector3(base.transform.position.x + (float)i * gameObject2.transform.localScale.x + num2, base.transform.position.y - (float)num * gameObject2.transform.localScale.y, base.transform.position.z);
            Debug.Log("position   :    : " + position);
            gameObject2.transform.parent = gameObject.transform;
            gameObject2.transform.position = position;
            GameObject[] array = GameObject.FindGameObjectsWithTag("Mesh");
            gameObject2.name += array.Length.ToString();
            gameObject2.GetComponent<Grid>().offset = num2;
            this.squares.Add(gameObject2);
        }
        this.lastRow = num;
    }

    public GameObject GetSquare(int row, int col)
    {
        return this.squares[row * creatorBall.columns + col];
    }

    public static creatorBall Instance;

    public GameObject ball_hd;

    public GameObject ball_ld;

    public GameObject bug_hd;

    public GameObject bug_ld;

    public GameObject thePrefab;

    private GameObject ball;

    private GameObject bug;

    public GameObject Ball_coin;

    public GameObject Ball_diamond;

    public GameObject Ball_fragment;

    private string[] ballsForCatapult = new string[11];

    private string[] ballsForMatrix = new string[11];

    private string[] bugs = new string[11];

    public static int columns = 9;

    public static int rows = 70;

    public static List<Vector2> grid = new List<Vector2>();

    private int lastRow;

    private float offsetStep = 0.33f;

    private GameObject Meshes;

    [HideInInspector]
    public List<GameObject> squares = new List<GameObject>();

    private int[] map;

    private int maxCols;

    private int maxRows;

    private LIMIT limitType;

    //public ball_type ballType;
}
