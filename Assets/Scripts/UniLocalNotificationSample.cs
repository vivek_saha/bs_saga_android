using System;
using System.Collections;
using Assets.SimpleAndroidNotifications;
using UnityEngine;

public class UniLocalNotificationSample : MonoBehaviour
{
	public static UniLocalNotificationSample instance { get; set; }

	private void Awake()
	{
		UniLocalNotificationSample.instance = this;
		MonoBehaviour.print("loacl notifiactions==========>");
		//NotificationManager.CancelAll();
		base.StartCoroutine(this.ScheduleNormal());
	}

	private IEnumerator ScheduleNormal()
	{
		int timeDelay = 0;
		for (int i = 1; i <= 100; i++)
		{
			timeDelay += 259200;
			//NotificationManager.SendWithAppIcon(TimeSpan.FromSeconds((double)timeDelay), " " + Application.productName, "It's been so long since you have played " + Application.productName + " Tap here to play now ", new Color(0f, 0.6f, 1f), NotificationIcon.Event);
			yield return new WaitForEndOfFrame();
		}
		yield break;
	}

	public IEnumerator ScheduleDailyReward()
	{
		MonoBehaviour.print("=================>  daily reward");
		int timeDelay = 0;
		for (int i = 1; i <= 100; i++)
		{
			timeDelay += 86400;
			//NotificationManager.SendWithAppIcon(TimeSpan.FromSeconds((double)timeDelay), " " + Application.productName, "Daily Rewared is filled " + Application.productName + " Tap here to play now ", new Color(0f, 0.6f, 1f), NotificationIcon.Event);
			yield return new WaitForEndOfFrame();
		}
		yield break;
	}
}
