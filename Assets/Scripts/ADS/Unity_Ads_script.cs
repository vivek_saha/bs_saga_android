﻿using UnityEngine;
using UnityEngine.Advertisements;

public class Unity_Ads_script : MonoBehaviour, IUnityAdsListener
{
    //public static Unity_Ads_script Instance;

    public string gameId = "4262269";
    public bool testMode = false;
    private string mySurfacingId = "Rewarded_Android";

    public bool interstitial_ad = false;

    public void Manual_Start()
    {
#if UNITY_ANDROID

        gameId = Ads_API.Instance.android_unity_game_id;
#elif UNITY_IOS
        gameId = Ads_API.Instance.ios_unity_game_id;
#else
        gameId = Ads_API.Instance.android_unity_game_id;
#endif

        if(gameId== "4262269")
        {
            testMode = true;
        }
        else
        {
            testMode = false;
        }
        // Initialize the Ads service:
        if (gameId != null)
        {
            Advertisement.AddListener(this);
            Advertisement.Initialize(gameId, testMode);
        }//ShowInterstitialAd();
        //StartCoroutine(show_temp_ad());
    }

    //IEnumerator show_temp_ad()
    //{
    //    yield return new WaitForSeconds(3f);
    //    //ShowInterstitialAd();
    //    ShowRewardedVideo();
    //}

    public void ShowInterstitialAd()
    {
        interstitial_ad = true;
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
        }
        else
        {
            Debug.Log("Interstitial ad not ready at the moment! Please try again later!");
        }
    }

    public bool check_unity_interstatial()
    {
        return Advertisement.IsReady();
    }

    public bool check_unity_rewarded()
    {
        return Advertisement.IsReady(mySurfacingId);
    }

    public void ShowRewardedVideo()
    {
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady(mySurfacingId))
        {
            //transform.GetComponent<Admob>().en
            Advertisement.Show(mySurfacingId);
        }
        else
        {
            Debug.Log("Rewarded video is not ready at the moment! Please try again later!");
        }
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish(string surfacingId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            // Reward the user for watching the ad to completion.
            //Debug.Log("got_reward");
            if (interstitial_ad)
            {
                interstitial_ad = false;
            }
            else
            {
                //transform.GetComponent<AdsController>().Reward_money_Add();
                Ads_priority_script.Instance.Add_reward();
                transform.GetComponent<Admob>().REquest_ads();
                transform.GetComponent<Admob>().Disable_admob_bg();
            }
            //interstitial_ad = false;
        }
        else if (showResult == ShowResult.Skipped)
        {
            transform.GetComponent<Admob>().REquest_ads();
            transform.GetComponent<Admob>().Disable_admob_bg();
            interstitial_ad = false;
            // Do not reward the user for skipping the ad.
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error.");
            interstitial_ad = false;
        }
    }

    public void OnUnityAdsReady(string surfacingId)
    {
        // If the ready Ad Unit or legacy Placement is rewarded, show the ad:
        if (surfacingId == mySurfacingId)
        {
            // Optional actions to take when theAd Unit or legacy Placement becomes ready (for example, enable the rewarded ads button)
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string surfacingId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }

    // When the object that subscribes to ad events is destroyed, remove the listener:
    public void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }
}