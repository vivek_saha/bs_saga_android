using System;
using InitScriptName;
using UnityEngine;
using UnityEngine.UI;

public class LIFESAddCounter : MonoBehaviour
{
	private void Start()
	{
		this.text = base.GetComponent<Text>();
	}

	private bool CheckPassedTime()
	{
		if (InitScript.DateOfExit == string.Empty || InitScript.DateOfExit == default(DateTime).ToString())
		{
			InitScript.DateOfExit = DateTime.Now.ToString();
		}
		DateTime value = DateTime.Parse(InitScript.DateOfExit);
		if (DateTime.Now.Subtract(value).TotalSeconds > (double)(this.TotalTimeForRestLife * (float)(InitScript.CapOfLife - InitScript.Lifes)))
		{
			InitScript.Instance.RestoreLifes();
			InitScript.RestLifeTimer = 0f;
			return false;
		}
		this.TimeCount((float)DateTime.Now.Subtract(value).TotalSeconds);
		return true;
	}

	private void TimeCount(float tick)
	{
		if (InitScript.RestLifeTimer <= 0f)
		{
			this.ResetTimer();
		}
		InitScript.RestLifeTimer -= tick;
		if (InitScript.RestLifeTimer <= 1f && InitScript.Lifes < InitScript.CapOfLife)
		{
			InitScript.Instance.AddLife(1);
			this.ResetTimer();
		}
	}

	private void ResetTimer()
	{
		InitScript.RestLifeTimer = this.TotalTimeForRestLife;
	}

	private void Update()
	{
		if (!this.startTimer && DateTime.Now.Subtract(DateTime.Now).Days == 0)
		{
			InitScript.DateOfRestLife = DateTime.Now;
			if (InitScript.Lifes < InitScript.CapOfLife && this.CheckPassedTime())
			{
				this.startTimer = true;
			}
		}
		if (this.startTimer)
		{
			this.TimeCount(Time.deltaTime);
		}
		if (base.gameObject.activeSelf)
		{
			if (InitScript.Lifes < InitScript.CapOfLife)
			{
				int num = Mathf.FloorToInt(InitScript.RestLifeTimer / 60f);
				int num2 = Mathf.FloorToInt(InitScript.RestLifeTimer - (float)(num * 60));
				this.text.enabled = true;
				this.text.text = string.Empty + string.Format("{0:00}:{1:00}", num, num2);
				InitScript.timeForReps = this.text.text;
			}
			else
			{
				this.text.text = "FULL";
			}
		}
	}

	private void OnApplicationPause(bool pauseStatus)
	{
		if (pauseStatus)
		{
			InitScript.DateOfExit = DateTime.Now.ToString();
		}
		else
		{
			this.startTimer = false;
		}
	}

	private void OnEnable()
	{
		this.startTimer = false;
	}

	private void OnDisable()
	{
		InitScript.DateOfExit = DateTime.Now.ToString();
	}

	private Text text;

	private static float TimeLeft;

	private float TotalTimeForRestLife = 900f;

	private bool startTimer;

	private DateTime templateTime;
}
