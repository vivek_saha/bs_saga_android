﻿using SimpleJSON;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Login_Api_script : Singleton<Login_Api_script>
{

    //public static Login_Api_script I;
    //public string login_type;
    //public int current_xp;
    //public string fb_id;
    //public string email;
    //public string Name;
    //public int wallet_coin;
    //public string profile_picture_link;
    private string device_token;
    private string Token_autorization;
    private JSONNode jsonResult;

    string rawJson;








    private void Awake()
    {
        //if (I != null && I != this)
        //{
        //    Destroy(this.gameObject);
        //}

        //else
        //{
        //    I = this;
        //DontDestroyOnLoad(this.gameObject);

        //}
        //I = this;
        //DontDestroyOnLoad(this.gameObject);
    }
    // Start is called before the first frame update
    public void Login_API_Start()
    {
        device_token = SystemInfo.deviceUniqueIdentifier;

        //Start_upload();
        if (PlayerPrefs.GetString("Token", string.Empty) == string.Empty)
        {
            Start_upload();

        }
        else
        {

            Setting_API.Instance.Get_settings_data();
        }
    }

    public void Start_upload()
    {
        //Debug.Log(fb_id);
        StartCoroutine("Upload");
    }

    IEnumerator Upload()
    {
        //Debug.Log("device_token" + device_token);
        WWWForm form = new WWWForm();
        form.AddField("login_type", "guest");
        //form.AddField("name", Name);
        Debug.Log(device_token);
        form.AddField("device_token", device_token);
        //if (login_type == "facebook")
        //{
        //    form.AddField("fb_id", fb_id);
        //    form.AddField("email", email);
        //    //form.AddField("profile_picture", profile_picture_link);
        //}

        form.AddField("level", 1);
        form.AddField("goldcoin", 0);
        form.AddField("diamond", 0);
        //form.AddBinaryData("profile_picture", edit_profile_panel.I.itemBGBytes, "temp.png", "image/png");


        UnityWebRequest www = UnityWebRequest.Post(Firebase_custome_script.Instance.server_link + "/api/login", form);
        yield return www.SendWebRequest();
        Debug.Log("www: " + www.responseCode);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            //Check_Internet_connection();
        }
        else
        {
            if (Notice_panel_script.Instance != null)
            {
                close_Retry_panel();
            }
            rawJson = www.downloadHandler.text;
            Get_response_data();
            Debug.Log("Form upload complete!" + rawJson);
        }

    }

    public void Get_response_data()
    {
        Debug.Log(rawJson);
        jsonResult = JSON.Parse(rawJson);


        Token_autorization = "Bearer " + jsonResult["data"]["token"].Value;
        PlayerPrefs.SetString("Token", Token_autorization);
        Debug.Log("Token_autorization : " + Token_autorization);
        //GetComponent<Setting_api_script>().manual_start();
        Setting_API.Instance.Get_settings_data();
    }



    // Update is called once per frame
    void Update()
    {

    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    //Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.Something_wentwrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    //Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.No_internet_Panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.No_internet_Panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }

    public void Retry_connection()
    {
        Start_upload();

        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        //Notice_panel_script.Instance.notice_Panel.SetActive(false);
        Start_upload();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        //Notice_panel_script.Instance.notice_Panel.SetActive(false);
    }

    protected override void OnApplicationQuitCallback()
    {
        //throw new NotImplementedException();
    }

    protected override void OnEnableCallback()
    {
        //throw new NotImplementedException();
    }
}
