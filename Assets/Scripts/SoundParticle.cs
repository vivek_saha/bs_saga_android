using System;
using UnityEngine;

public class SoundParticle : MonoBehaviour
{
	private void Start()
	{
	}

	public void Stop()
	{
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.swish[0]);
	}

	public void Hit()
	{
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.hit);
	}
}
