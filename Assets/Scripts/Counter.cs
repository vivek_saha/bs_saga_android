using InitScriptName;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    private void Start()
    {
        this.label = base.GetComponent<Text>();
    }

    private void Update()
    {
        if (base.name == "Moves")
        {
            this.label.text = string.Empty + LevelData.LimitAmount;
            if (LevelData.LimitAmount <= 5f && GamePlay.Instance.GameStatus == GameState.Playing)
            {
                this.label.color = Color.red;
                if (!base.GetComponent<Animation>().isPlaying)
                {
                    base.GetComponent<Animation>().Play();
                    SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.alert);
                }
            }
        }
        if (base.name == "Scores" || base.name == "Score")
        {
            this.label.text = string.Empty + mainscript.Score;
        }
        if (base.name == "Score")
        {
            this.label.text = "Score : " + mainscript.Score;
        }
        if (base.name == "Level")
        {
            this.label.text = "Level " + PlayerPrefs.GetInt("OpenLevel");
            //Debug.Log("Hrer ?????????????????????????????????????");Flabel
        }
        if (base.name == "Level_home")
        {
            this.label.text = "Level : " + PlayerPrefs.GetInt("OpenLevel");
            //Debug.Log("Hrer ?????????????????????????????????????");Flabel
        }
        if (base.name == "Target")
        {
            if (LevelData.mode == ModeGame.Vertical)
            {
                this.label.text = string.Empty + Mathf.Clamp(mainscript.Instance.TargetCounter1, 0, 6) + "/6";
            }
            else if (LevelData.mode == ModeGame.Rounded)
            {
                this.label.text = string.Empty + Mathf.Clamp(mainscript.Instance.TargetCounter1, 0, 1) + "/1";
            }
            else if (LevelData.mode == ModeGame.Animals)
            {
                this.label.text = string.Concat(new object[]
                {
                    string.Empty,
                    mainscript.Instance.TargetCounter1,
                    "/",
                    mainscript.Instance.TotalTargets
                });
            }
        }
        if (base.name == "Lifes")
        {
            this.label.text = string.Empty + InitScript.Instance.GetLife();
            if (base.GetComponent<Text>().text == "5")
            {
                base.transform.parent.GetChild(4).GetComponent<Button>().interactable = false;
            }
            else
            {
                base.transform.parent.GetChild(4).GetComponent<Button>().interactable = true;
            }
        }
        if (base.name == "Gems")
        {
            this.label.text = string.Empty + PlayerPrefs.GetInt("Gems");
        }
        if (base.name == "Diamond_Value")
        {
            this.label.text = string.Empty + PlayerPrefs.GetFloat("Diamonds");
        }
        if (base.name == "BombBallBoost")
        {
            if (InitScript.Instance.BombBallBoost == 0)
            {
                GetPlus();
            }
            else
            {
                transform.GetComponentInParent<Image>().sprite = mainscript.Instance.count_boost_;
                
                this.label.text = string.Empty + InitScript.Instance.BombBallBoost;
            }
        }
        if (base.name == "ColorBallBoost")
        {
            if (InitScript.Instance.ColorBallBoost == 0)
            {
                GetPlus();
            }
            else
            {
                transform.GetComponentInParent<Image>().sprite = mainscript.Instance.count_boost_;
                this.label.text = string.Empty + InitScript.Instance.ColorBallBoost;
            }
        }
        if (base.name == "FireBallBoost")
        {
            if (InitScript.Instance.FireBallBoost == 0)
            {
                GetPlus();
            }
            else
            {
                transform.GetComponentInParent<Image>().sprite = mainscript.Instance.count_boost_;
                this.label.text = string.Empty + InitScript.Instance.FireBallBoost;
            }
        }
        if (base.name == "LineBallBoost")
        {
            if (InitScript.Instance.LineBallBoost == 0)
            {
                GetPlus();
            }
            else
            {
                transform.GetComponentInParent<Image>().sprite = mainscript.Instance.count_boost_;
                this.label.text = string.Empty + InitScript.Instance.LineBallBoost;
            }
        }
        if (base.name == "TargetDescription")
        {
            this.label.text = string.Empty + this.GetTarget();
        }
        if (base.name == "Price")
        {
            if (GamePlay.GamePreover == 1)
            {
                base.GetComponent<Text>().text = "Play With 5";
            }
            else if (GamePlay.GamePreover == 2)
            {
                base.GetComponent<Text>().text = "Play With 7";
            }
            else if (GamePlay.GamePreover == 3)
            {
                base.GetComponent<Text>().text = "Play With 10";
            }
            else
            {
                base.GetComponent<Text>().text = "Play With 10";
            }
        }
        if (base.name == "AddMovestext")
        {
            if (GamePlay.GamePreover == 1)
            {
                base.GetComponent<Text>().text = "Add <b> 5 </b> Moves To continue !";
            }
            else if (GamePlay.GamePreover == 2)
            {
                base.GetComponent<Text>().text = "Add <b> 10 </b> Moves To continue !";
            }
            else if (GamePlay.GamePreover == 3)
            {
                base.GetComponent<Text>().text = "Add <b> 13 </b> Moves To continue !";
            }
            else
            {
                base.GetComponent<Text>().text = "Add <b> 13 </b> Moves To continue !";
            }
        }
    }

    private void GetPlus()
    {
        //if (boostCount > 0)
        //{
        //    return string.Empty + boostCount;
        //}
        //return "+";
        transform.GetComponentInParent<Image>().sprite = mainscript.Instance.plus_boost_;
        base.GetComponent<Text>().text = "";


    }

    private string GetTarget()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "map")
        {
            if (InitScript.Instance.currentTarget == Target.Top)
            {
                return "Clear the top";
            }
            if (InitScript.Instance.currentTarget == Target.Chicken)
            {
                return "Rescue the Puppy";
            }
            return "Save the Puppy";
        }
        else
        {
            if (LevelData.mode == ModeGame.Vertical)
            {
                return "Clear the top";
            }
            if (LevelData.mode == ModeGame.Rounded)
            {
                return "Rescue the Puppy";
            }
            return "Save the Puppy";
        }
    }

    private Text label;
}
