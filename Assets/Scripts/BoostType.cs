using System;

public enum BoostType
{
	ColorBallBoost,
	FireBallBoost,
	LineBallBoost,
	BombBallBoost
}
