using InitScriptName;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class mainscript : MonoBehaviour
{
    public static int Score
    {
        get
        {
            return mainscript.score;
        }
        set
        {
            mainscript.score = value;
        }
    }

    public int ComboCount
    {
        get
        {
            return this._ComboCount;
        }
        set
        {
            this._ComboCount = value;
            if (value > 0)
            {
                SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.combo[Mathf.Clamp(value - 1, 0, 5)]);
                creatorBall.Instance.CreateBug(mainscript.lastBall, value);
                if (value >= 6)
                {
                    //if (PlayerPrefs.GetInt("sound", 1) == 1)
                    //{
                    //    SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.combo[5]);
                    //}
                    //base.StartCoroutine(this.DoubleScoreParticle());
                    //this.FireEffect.SetActive(true);
                    //mainscript.doubleScore = 2;
                }
            }
            else
            {
                this.DestroyBugs();
                this.FireEffect.SetActive(false);
                mainscript.doubleScore = 1;
            }
        }
    }

    private IEnumerator DoubleScoreParticle()
    {
        if (this.onetimeEntry)
        {
            this.onetimeEntry = false;
            for (int i = 0; i < this.DoubleBonus.Length; i++)
            {
                this.DoubleBonus[i].SetActive(true);
                SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.Fireworks);
                yield return new WaitForSeconds(0.3f);
            }
        }
        yield break;
    }

    public int TargetCounter1
    {
        get
        {
            return this.TargetCounter;
        }
        set
        {
            this.TargetCounter = value;
        }
    }

    private void Awake()
    {
        PlayerPrefs.SetString("Scene", "game");
        if (InitScript.Instance == null)
        {
            base.gameObject.AddComponent<InitScript>();
        }
        this.currentLevel = PlayerPrefs.GetInt("OpenLevel");
        mainscript.stage = 1;
        mainscript.StopControl = false;
        this.animTable.Clear();
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
        }
        this.creatorBall = GameObject.Find("Creator").GetComponent<creatorBall>();
        base.StartCoroutine(this.CheckColors());
        this.Layer.SetActive(false);
        float num = (float)Screen.height / (float)Screen.width;
        if (num > 2f)
        {
            base.GetComponent<Camera>().orthographicSize = 8f;
            GameObject.Find("Background").transform.localScale = new Vector3(1.054873f, 1.3f, 1.054873f);
            GameObject.Find("Background").transform.localPosition = new Vector3(0f, 0f, 17.55f);
        }
        else if (num == 2f)
        {
            base.GetComponent<Camera>().orthographicSize = 7.4f;
            GameObject.Find("Background").transform.localScale = new Vector3(1.054873f, 1.3f, 1.054873f);
            GameObject.Find("Background").transform.localPosition = new Vector3(0f, 0f, 17.55f);
        }
        else if (num < 2f && num > 1.6f)
        {
            base.GetComponent<Camera>().orthographicSize = 6.6f;
        }
        else if (num < 1.6f)
        {
            base.GetComponent<Camera>().orthographicSize = 6.6f;
        }
    }

    private IEnumerator CheckColors()
    {
        for (; ; )
        {
            this.GetColorsInGame();
            yield return new WaitForEndOfFrame();
            this.SetColorsForNewBall();
        }
        yield break;
    }

    private IEnumerator ShowArrows()
    {
        for (; ; )
        {
            yield return new WaitForSeconds(30f);
            if (GamePlay.Instance.GameStatus == GameState.Playing)
            {
                this.arrows.SetActive(true);
            }
            yield return new WaitForSeconds(3f);
            this.arrows.SetActive(false);
        }
        yield break;
    }

    private void DestroyBugs()
    {
        Transform transform = GameObject.Find("Spiders").transform;
        List<Bug> list = new List<Bug>();
        for (int i = 0; i < 2; i++)
        {
            list.Clear();
            IEnumerator enumerator = transform.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    object obj = enumerator.Current;
                    Transform transform2 = (Transform)obj;
                    if (transform2.childCount > 0)
                    {
                        list.Add(transform2.GetChild(0).GetComponent<Bug>());
                    }
                }
            }
            finally
            {
                IDisposable disposable;
                if ((disposable = (enumerator as IDisposable)) != null)
                {
                    disposable.Dispose();
                }
            }
            if (list.Count > 0)
            {
                list[UnityEngine.Random.Range(0, list.Count)].MoveOut();
            }
        }
    }

    public void PopupScore(int value, Vector3 pos)
    {
        mainscript.Score += value;
        Transform transform = GameObject.Find("CanvasScore").transform;
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.popupScore, pos, Quaternion.identity);
        gameObject.transform.GetComponentInChildren<Text>().text = string.Empty + value;
        gameObject.transform.SetParent(transform);
        gameObject.transform.localScale = Vector3.one;
        UnityEngine.Object.Destroy(gameObject, 1f);
    }

    public void SwitchLianaBoost()
    {
        if (!mainscript.ElectricBoost)
        {
            mainscript.ElectricBoost = true;
            this.ElectricLiana.SetActive(true);
        }
        else
        {
            mainscript.ElectricBoost = false;
            this.ElectricLiana.SetActive(false);
        }
    }

    private void Start()
    {
        mainscript.Instance = this;
        this.stageTemp = 1;
        this.RandomizeWaitTime();
        mainscript.score = 0;
        if (PlayerPrefs.GetInt("noSound") == 1)
        {
            this.noSound = true;
        }
        GamePlay.Instance.GameStatus = GameState.BlockedGame;
        ball_container = GameObject.Find("-Ball");
        base.StartCoroutine(this.startinteraction());
    }

    private IEnumerator startinteraction()
    {
        //MonoBehaviour.print("---------->aaaaaaaaaaaaaaaa<--------------");
        yield return new WaitForSeconds(7f);
        GameObject.Find("CanvasMoves").transform.Find("GameObject").GetComponent<Button>().interactable = true;
        yield break;
    }

    private void Update()
    {
        if (this.noSound)
        {
            base.GetComponent<AudioSource>().volume = 0f;
        }
        if (!this.noSound)
        {
            base.GetComponent<AudioSource>().volume = 0.5f;
        }
        if (UnityEngine.Input.GetKeyDown(KeyCode.Escape) && GamePlay.Instance.GameStatus == GameState.Playing)
        {
            if (this.SettingsPanel.activeSelf)
            {
                this.SettingsPanel.SetActive(false);
            }
            if (this.BoostPanel.activeSelf)
            {
                this.BoostPanel.SetActive(false);
            }
            else if (this.PauseMenu.activeSelf)
            {
                this.PauseButtonPopOut(this.PauseMenu);
            }
            else
            {
                PauseMenu.SetActive(true);
            }
            //else if (this.ExitPanel.gameObject.activeSelf)
            //{
            //    this.ExitPanel.gameObject.SetActive(false);
            //}
            //else
            //{
            //    this.ExitPanel.gameObject.SetActive(true);
            //}
        }
        if (UnityEngine.Input.GetKeyDown(KeyCode.A))
        {
        }
        if (this.gameOver && !this.gameOverShown)
        {
            this.gameOverShown = true;
        }
        if (GamePlay.Instance.GameStatus == GameState.Win)
        {
        }
        if (this.checkBall != null && (GamePlay.Instance.GameStatus == GameState.Playing || GamePlay.Instance.GameStatus == GameState.WaitForChicken))
        {
            this.checkBall.GetComponent<ball>().checkNearestColor();
            UnityEngine.Object.Destroy(this.checkBall.GetComponent<Rigidbody>());
            LevelData.LimitAmount -= 1f;
            this.checkBall = null;
            int num = 1;
            if (mainscript.stage >= 3)
            {
                num = 2;
            }
            if (mainscript.stage >= 9)
            {
                num = 1;
            }
            base.StartCoroutine(this.destroyAloneBall());
            if (!this.arcadeMode)
            {
                if (this.bounceCounter >= num)
                {
                    this.bounceCounter = 0;
                    this.dropDownTime = Time.time + 0.5f;
                }
                else if (!this.destringAloneBall && !this.dropingDown)
                {
                    this.destringAloneBall = true;
                }
            }
        }
        if (this.arcadeMode && Time.time > this.ArcadedropDownTime && GamePlay.Instance.GameStatus == GameState.Playing)
        {
            this.bounceCounter = 0;
            this.ArcadedropDownTime = Time.time + 10f;
            this.dropDownTime = Time.time + 0.2f;
            this.dropDown();
        }
        if (Time.time > this.dropDownTime && this.dropDownTime != 0f)
        {
            this.CheckLosing();
            this.dropDownTime = 0f;
            base.StartCoroutine(this.getBallsForMesh());
        }
        ///win _change
        //if (LevelData.mode == ModeGame.Vertical && this.TargetCounter >= 6 && GamePlay.Instance.GameStatus == GameState.Playing)
        if (LevelData.mode == ModeGame.Vertical && ball_container.transform.childCount == 0 && GamePlay.Instance.GameStatus == GameState.Playing)
        {
            MonoBehaviour.print("============>");
            GamePlay.Instance.GameStatus = GameState.Win;
        }
        else if (LevelData.mode == ModeGame.Rounded && this.TargetCounter >= 1 && GamePlay.Instance.GameStatus == GameState.WaitForChicken)
        {
            GamePlay.Instance.GameStatus = GameState.Win;
        }
        else if (LevelData.mode == ModeGame.Animals && this.TargetCounter >= this.TotalTargets && GamePlay.Instance.GameStatus == GameState.Playing)
        {
            GamePlay.Instance.GameStatus = GameState.Win;
        }
        else if (LevelData.LimitAmount <= 0f && GamePlay.Instance.GameStatus == GameState.Playing && this.newBall == null && ball_container.transform.childCount != 0)
        {
            StartCoroutine("wait_for_loss");
            //Debug.Log("loss");
            //GamePlay.Instance.GameStatus = GameState.GameOver;
        }
        ProgressBarScript.Instance.UpdateDisplay((float)mainscript.score * 100f / ((float)LevelData.star1 / ((float)LevelData.star1 * 100f / (float)LevelData.star3) * 100f) / 100f);
        if (mainscript.score >= LevelData.star1 && this.stars <= 0)
        {
            this.stars = 1;
        }
        if (mainscript.score >= LevelData.star2 && this.stars <= 1)
        {
            this.stars = 2;
        }
        if (mainscript.score >= LevelData.star3 && this.stars <= 2)
        {
            this.stars = 3;
        }
        if (mainscript.score >= LevelData.star1 && !this.starsObject[0].activeSelf)
        {
            SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.Stars);
            this.starsObject[0].SetActive(true);
        }
        if (mainscript.score >= LevelData.star2 && !this.starsObject[1].activeSelf)
        {
            //MonoBehaviour.print("star========2>");
            SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.Stars);
            this.starsObject[1].SetActive(true);
        }
        if (mainscript.score >= LevelData.star3 && !this.starsObject[2].activeSelf)
        {
            //MonoBehaviour.print("star========3>");
            SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.Stars);
            this.starsObject[2].SetActive(true);
        }
    }

    public void CheckLosing()
    {
        this.fixedBalls = (UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) as GameObject[]);
        foreach (GameObject gameObject in this.fixedBalls)
        {
            if (gameObject.layer == 9)
            {
            }
        }
    }

    private IEnumerator startBonusLiana()
    {
        for (; ; )
        {
            yield return new WaitForSeconds((float)UnityEngine.Random.Range(30, 120));
            UnityEngine.Object.Instantiate<GameObject>(this.BonusLiana);
        }
        yield break;
    }

    private IEnumerator startBonusScore()
    {
        for (; ; )
        {
            yield return new WaitForSeconds((float)UnityEngine.Random.Range(5, 20));
            UnityEngine.Object.Instantiate<GameObject>(this.BonusScore);
        }
        yield break;
    }


    IEnumerator wait_for_loss()
    {
        yield return new WaitForSeconds(1.5f);
        if (ball_container.transform.childCount != 0)
        {
            GamePlay.Instance.GameStatus = GameState.GameOver;
        }
    }

    private IEnumerator startButterfly()
    {
        for (; ; )
        {
            yield return new WaitForSeconds((float)UnityEngine.Random.Range(5, 10));
            GameObject gm = GameObject.Find("Creator");
            this.revertButterFly *= -1;
            gm.GetComponent<creatorButterFly>().createButterFly(this.revertButterFly);
        }
        yield break;
    }

    private IEnumerator getBallsForMesh()
    {
        GameObject[] meshes = GameObject.FindGameObjectsWithTag("Mesh");
        foreach (GameObject gameObject in meshes)
        {
            Collider2D[] array2 = Physics2D.OverlapCircleAll(gameObject.transform.position, 0.1f, 512);
            foreach (Collider2D collider2D in array2)
            {
                gameObject.GetComponent<Grid>().Busy = collider2D.gameObject;
                collider2D.GetComponent<bouncer>().offset = gameObject.GetComponent<Grid>().offset;
            }
        }
        yield return new WaitForSeconds(0.2f);
        yield break;
    }

    public GameObject createFirstBall(Vector3 vector3)
    {
        GameObject gameObject = GameObject.Find("Creator");
        if (LevelData.LimitAmount > 2f || GamePlay.Instance.GameStatus == GameState.Win)
        {

            BallColor bc = new BallColor();
            GameObject a = GameObject.Find("-Ball");
            List<BallColor> az = new List<BallColor>();
            if (a != null)
            {
                int m;
                if (a.transform.childCount >= 3)
                {
                    m = 3;
                }
                else
                {
                    m = a.transform.childCount;
                }
                for (int i = 0; i < m; i++)
                {
                    az.Add(a.transform.GetChild(a.transform.childCount - 1 - i).GetComponent<ColorBallScript>().mainColor);
                }


                //if (az.Contains(busy.GetComponent<ColorBallScript>().mainColor))
                //{

                //}
                //else
                //{
                bc = az[UnityEngine.Random.Range(0, az.Count)];


                if (bc.ToString().Contains("coin"))
                {
                    bc = (BallColor)System.Enum.Parse(typeof(BallColor), bc.ToString().Substring(0, bc.ToString().Length - 4));
                }
                else if (bc.ToString().Contains("diamond"))
                {
                    bc = (BallColor)System.Enum.Parse(typeof(BallColor), bc.ToString().Substring(0, bc.ToString().Length - 7));
                }
                else if (bc.ToString().Contains("fragment"))
                {
                    bc = (BallColor)System.Enum.Parse(typeof(BallColor), bc.ToString().Substring(0, bc.ToString().Length - 8));
                }






                //}
            }
            return gameObject.GetComponent<creatorBall>().createBall(vector3, bc, true, 1, 0);
        }
        return gameObject;
    }

    public void connectNearBallsGlobal()
    {
        //this.fixedBalls = (UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) as GameObject[]);
        this.fixedBalls =new GameObject [ GameObject.Find("-Ball").transform.childCount];

        for (int i = 0; i < GameObject.Find("-Ball").transform.childCount; i++)
        {
            fixedBalls[i] = GameObject.Find("-Ball").transform.GetChild(i).gameObject;
        }
        foreach (GameObject gameObject in this.fixedBalls)
        {
            if (gameObject.layer == 9)
            {
                //if (gameObject.GetComponent<ball>() != null)
                    gameObject.GetComponent<ball>().connectNearBalls();
            }
        }
    }

    public void dropUp()
    {
        if (!this.dropingDown)
        {
            this.creatorBall.AddMesh();
            this.dropingDown = true;
            GameObject gameObject = GameObject.Find("-Meshes");
            iTween.MoveAdd(gameObject, iTween.Hash(new object[]
            {
                "y",
                0.5f,
                "time",
                0.3,
                "easetype",
                iTween.EaseType.linear,
                "onComplete",
                "OnMoveFinished"
            }));
        }
    }

    private void OnMoveFinished()
    {
        this.dropingDown = false;
    }

    public void dropDown()
    {
        this.dropingDown = true;
        this.fixedBalls = (UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) as GameObject[]);
        foreach (GameObject gameObject in this.fixedBalls)
        {
            if (gameObject.layer == 9)
            {
                gameObject.GetComponent<bouncer>().dropDown();
            }
        }
        GameObject gameObject2 = GameObject.Find("Creator");
        gameObject2.GetComponent<creatorBall>().createRow(0);
    }

    public void explode(GameObject gameObject)
    {
    }

    private void RandomizeWaitTime()
    {
        this.waitTime = Time.time + UnityEngine.Random.Range(5f, 10f);
    }

    public IEnumerator destroyAloneBall()
    {
        mainscript.Instance.newBall2 = null;
        yield return new WaitForSeconds(Mathf.Clamp((float)this.countOfPreparedToDestroy / 50f, 0.6f, (float)this.countOfPreparedToDestroy / 50f));
        this.connectNearBallsGlobal();
        int i = 0;
        int willDestroy = 0;
        this.destringAloneBall = true;
        Camera.main.GetComponent<mainscript>().arraycounter = 0;
        GameObject[] fixedBalls = UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) as GameObject[];
        Camera.main.GetComponent<mainscript>().controlArray.Clear();
        //Debug.Log()

        List<GameObject> gameObjectList = new List<GameObject>(fixedBalls);
        gameObjectList.RemoveAll(x => x == null);
        fixedBalls = gameObjectList.ToArray();
        //var objects = fixedBalls.Where(f => f != null);
        foreach (GameObject obj in fixedBalls)
        {
            if (obj.GetComponent<ball>() != null)
            {
                if (obj != null && obj.layer == 9 && !this.findInArray(Camera.main.GetComponent<mainscript>().controlArray, obj.gameObject) && obj.GetComponent<ball>().nearBalls.Count < 7 && obj.GetComponent<ball>().nearBalls.Count > 0)
                {
                    i++;
                    yield return new WaitForEndOfFrame();
                    ArrayList b = new ArrayList();
                    obj.GetComponent<ball>().checkNearestBall(b);
                    if (b.Count > 0)
                    {
                        willDestroy++;
                        this.destroy(b);
                    }
                }
            }
        }
        this.destringAloneBall = false;
        base.StartCoroutine(this.getBallsForMesh());
        this.dropingDown = false;
        if (LevelData.mode == ModeGame.Vertical)
        {
            creatorBall.Instance.MoveLevelDown();
        }
        else if (LevelData.mode == ModeGame.Animals)
        {
            creatorBall.Instance.MoveLevelDown();
        }
        else if (LevelData.mode == ModeGame.Rounded)
        {
            this.CheckBallsBorderCross();
        }
        yield return new WaitForSeconds(0f);
        this.GetColorsInGame();
        mainscript.Instance.newBall = null;
        this.SetColorsForNewBall();
        yield break;
    }

    public void SetColorsForNewBall()
    {
        //Debug.Log( "colordict"  +mainscript.colorsDict.Count);
        if (this.boxCatapult.GetComponent<Grid>().Busy != null && mainscript.colorsDict.Count > 0)
        {
            GameObject busy = this.boxCatapult.GetComponent<Grid>().Busy;
            //BallColor mainColor = busy.GetComponent<ColorBallScript>().mainColor;
            //if (!mainscript.colorsDict.ContainsValue(mainColor))
            //{
            //    busy.GetComponent<ColorBallScript>().SetColor(mainscript.colorsDict[UnityEngine.Random.Range(0, mainscript.colorsDict.Count)]);
            //}


            GameObject a = GameObject.Find("-Ball");
            List<BallColor> az = new List<BallColor>();
            if (a != null)
            {
                int m;
                if (a.transform.childCount >= 3)
                {
                    m = 3;
                }
                else
                {
                    m = a.transform.childCount;
                }
                for (int i = 0; i < m; i++)
                {
                    az.Add(a.transform.GetChild(a.transform.childCount - 1 - i).GetComponent<ColorBallScript>().mainColor);
                }


                if (az.Contains(busy.GetComponent<ColorBallScript>().mainColor))
                {

                }
                else
                {
                    if (az.Count > 0)
                        busy.GetComponent<ColorBallScript>().SetColor(az[UnityEngine.Random.Range(0, az.Count)]);
                }
            }





        }
    }

    public void GetColorsInGame()
    {
        int num = 0;
        mainscript.colorsDict.Clear();
        IEnumerator enumerator = this.Balls.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                object obj = enumerator.Current;
                Transform transform = (Transform)obj;
                if (!(transform.tag == "chicken") && !(transform.tag == "empty") && !(transform.tag == "Ball") && !(transform.tag == "bomb") && !(transform.tag == "light"))
                {
                    BallColor ballColor = (BallColor)Enum.Parse(typeof(BallColor), transform.tag);
                    //if(ballColor.ToString().Contains("fragment"))
                    //{

                    //}
                    if (!mainscript.colorsDict.ContainsValue(ballColor) && ballColor <= BallColor.random)
                    {
                        mainscript.colorsDict.Add(num, ballColor);
                        num++;
                    }
                }
            }
        }
        finally
        {
            IDisposable disposable;
            if ((disposable = (enumerator as IDisposable)) != null)
            {
                disposable.Dispose();
            }
        }
    }

    public void CheckFreeChicken()
    {
        if (LevelData.mode != ModeGame.Rounded)
        {
            return;
        }
        if (GamePlay.Instance.GameStatus == GameState.Playing)
        {
            base.StartCoroutine(this.CheckFreeChickenCor());
        }
    }

    private IEnumerator CheckFreeChickenCor()
    {
        GamePlay.Instance.GameStatus = GameState.WaitForChicken;
        yield return new WaitForSeconds(1.5f);
        bool finishGame = false;
        if (LevelData.mode == ModeGame.Rounded)
        {
            finishGame = true;
            GameObject gameObject = GameObject.Find("-Ball");
            IEnumerator enumerator = gameObject.transform.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    object obj = enumerator.Current;
                    Transform transform = (Transform)obj;
                    if (transform.tag != "Ball" && transform.tag != "chicken")
                    {
                        finishGame = false;
                    }
                }
            }
            finally
            {
                IDisposable disposable;
                if ((disposable = (enumerator as IDisposable)) != null)
                {
                    disposable.Dispose();
                }
            }
        }
        if (!finishGame)
        {
            this.GetColorsInGame();
            GamePlay.Instance.GameStatus = GameState.Playing;
        }
        else if (finishGame)
        {
            GamePlay.Instance.GameStatus = GameState.WaitForChicken;
            GameObject chicken = GameObject.FindGameObjectWithTag("chicken");
            chicken.GetComponent<SpriteRenderer>().sortingLayerName = "UI Layer";
            Vector3 targetPos = new Vector3(2.3f, 6f, 0f);
            mainscript.Instance.TargetCounter++;
            AnimationCurve curveX = new AnimationCurve(new Keyframe[]
            {
                new Keyframe(0f, chicken.transform.position.x),
                new Keyframe(0.5f, targetPos.x)
            });
            AnimationCurve curveY = new AnimationCurve(new Keyframe[]
            {
                new Keyframe(0f, chicken.transform.position.y),
                new Keyframe(0.5f, targetPos.y)
            });
            curveY.AddKey(0.2f, chicken.transform.position.y - 1f);
            float startTime = Time.time;
            Vector3 startPos = chicken.transform.position;
            float distCovered = 0f;
            while (distCovered < 0.6f)
            {
                distCovered = Time.time - startTime;
                chicken.transform.position = new Vector3(curveX.Evaluate(distCovered), curveY.Evaluate(distCovered), 0f);
                chicken.transform.Rotate(Vector3.back * 10f);
                yield return new WaitForEndOfFrame();
            }
            UnityEngine.Object.Destroy(chicken);
        }
        yield break;
    }

    private void CheckBallsBorderCross()
    {
        IEnumerator enumerator = this.Balls.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                object obj = enumerator.Current;
                Transform transform = (Transform)obj;
                transform.GetComponent<ball>().CheckBallCrossedBorder();
            }
        }
        finally
        {
            IDisposable disposable;
            if ((disposable = (enumerator as IDisposable)) != null)
            {
                disposable.Dispose();
            }
        }
    }

    public bool findInArray(ArrayList b, GameObject destObj)
    {
        IEnumerator enumerator = b.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                object obj = enumerator.Current;
                GameObject x = (GameObject)obj;
                if (x == destObj)
                {
                    return true;
                }
            }
        }
        finally
        {
            IDisposable disposable;
            if ((disposable = (enumerator as IDisposable)) != null)
            {
                disposable.Dispose();
            }
        }
        return false;
    }

    public void destroy(GameObject obj)
    {
        if (obj.name.IndexOf("ball") == 0)
        {
            obj.layer = 0;
        }
        Camera.main.GetComponent<mainscript>().bounceCounter = 0;
        obj.GetComponent<ball>().Destroyed = true;
        obj.GetComponent<ball>().growUp();
        Camera.main.GetComponent<mainscript>().explode(obj.gameObject);
    }

    private void playPop()
    {
    }

    public void destroy(ArrayList b)
    {
        Camera.main.GetComponent<mainscript>().bounceCounter = 0;
        int num = 0;
        int num2 = 0;
        IEnumerator enumerator = b.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                object obj = enumerator.Current;
                GameObject gameObject = (GameObject)obj;
                if (gameObject.name.IndexOf("ball") == 0)
                {
                    gameObject.layer = 0;
                }
                if (!gameObject.GetComponent<ball>().Destroyed)
                {
                    if (num > 3)
                    {
                        num2 += 3;
                        num += num2;
                    }
                    num++;
                    gameObject.GetComponent<ball>().StartFall();
                }
            }
        }
        finally
        {
            IDisposable disposable;
            if ((disposable = (enumerator as IDisposable)) != null)
            {
                disposable.Dispose();
            }
        }
        this.CheckFreeChicken();
    }

    public void ChangeBoost()
    {
        //MonoBehaviour.print("==========level amount====" + LevelData.LimitAmount);
        if (this.BallChanged && this.BallReady && UnityEngine.Input.touchCount == 1 && LevelData.LimitAmount >= 2f)
        {
            this.BallReady = false;
            SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.swish[0]);
            Grid.waitForAnim = true;
            GameObject busy = this.boxSecond.GetComponent<Grid>().Busy;
            this.boxCatapult.GetComponent<Grid>().Busy.GetComponent<ball>().newBall = false;
            //this.boxSecond.GetComponent<Grid>().Busy.gameObject.transform.localScale =new Vector3(1f,1f);
            iTween.MoveTo(this.boxSecond.GetComponent<Grid>().Busy, iTween.Hash(new object[]
            {
                "position",
                this.boxCatapult.transform.position,
                "time",
                0.3,
                "easetype",
                iTween.EaseType.linear,
                "onComplete",
                "newBall"
            }));
            this.boxSecond.GetComponent<Grid>().Busy.gameObject.transform.localScale = new Vector3(1f, 1f);
            iTween.MoveTo(this.boxCatapult.GetComponent<Grid>().Busy, iTween.Hash(new object[]
            {
                "position",
                this.boxSecond.transform.position,
                "time",
                0.3,
                "easetype",
                iTween.EaseType.linear
            }));
            this.boxCatapult.GetComponent<Grid>().Busy.gameObject.transform.localScale = new Vector3(0.6f, 0.6f);
            this.boxSecond.GetComponent<Grid>().Busy = this.boxCatapult.GetComponent<Grid>().Busy;
            this.boxCatapult.GetComponent<Grid>().Busy = busy;
            base.StartCoroutine(this.BallChanging());
        }
    }

    private IEnumerator BallChanging()
    {
        yield return new WaitForSeconds(0.5f);
        this.BallReady = true;
        yield break;
    }

    public void destroyAllballs()
    {
        IEnumerator enumerator = this.Balls.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                object obj = enumerator.Current;
                Transform transform = (Transform)obj;
                if (transform.tag != "chicken")
                {
                    this.destroy(transform.gameObject);
                }
            }
        }
        finally
        {
            IDisposable disposable;
            if ((disposable = (enumerator as IDisposable)) != null)
            {
                disposable.Dispose();
            }
        }
        this.CheckFreeChicken();
    }

    public void PauseButtonPopIn(GameObject game)
    {
        game.SetActive(true);
        game.GetComponent<Animator>().enabled = true;
        game.GetComponent<Animator>().Play("PauseMenuPopIN", -1, 0f);
        GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().enabled = true;
        GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAnim", -1, 0f);
    }

    public void PauseButtonPopOut(GameObject game)
    {
        if (!this.PauseAnimRunning)
        {
            this.PauseAnimRunning = true;
            game.GetComponent<Animator>().Play("PauseMenuPopOut", -1, 0f);
            base.StartCoroutine(this.PauseButtonPopOutCorutine(game));
        }
    }

    private IEnumerator PauseButtonPopOutCorutine(GameObject game)
    {
        yield return new WaitForSeconds(0.6f);
        this.PauseAnimRunning = false;
        game.SetActive(false);
        GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAniOut", -1, 0f);
        yield break;
    }

    public void Set_false_layer_Function()
    {
        base.StartCoroutine(this.Set_false_layer1());
    }

    public IEnumerator Set_false_layer()
    {
        yield return new WaitForSeconds(1f);
        //MonoBehaviour.print("11111111----------true");
        mainscript.Instance.Layer.SetActive(false);
        yield break;
    }

    public IEnumerator Set_false_layer1()
    {
        yield return new WaitForSeconds(2f);
        //MonoBehaviour.print("11111111----------true");
        mainscript.Instance.Layer.SetActive(false);
        yield break;
    }

    public int currentLevel;

    public static mainscript Instance;

    private GameObject ball;

    private Vector2 speed = new Vector2(250f, 250f);

    private GameObject PauseDialogLD;

    private GameObject OverDialogLD;

    private GameObject PauseDialogHD;

    private GameObject OverDialogHD;

    private GameObject UI_LD;

    private GameObject UI_HD;

    private GameObject PauseDialog;

    private GameObject OverDialog;

    private GameObject FadeLD;

    private GameObject FadeHD;

    private GameObject AppearLevel;

    private Target target;

    private Vector2 worldPos;

    private Vector2 startPos;

    private float startTime;

    private float duration = 1f;

    private bool setTarget;

    private float mTouchOffsetX;

    private float mTouchOffsetY;

    private float xOffset;

    private float yOffset;

    public int bounceCounter;

    public GameObject[] fixedBalls;

    public Vector2[][] meshArray;

    private int offset;

    public GameObject checkBall;

    public GameObject newBall;

    private float waitTime;

    private int revertButterFly = 1;

    private static int score;

    public static int stage = 1;

    private const int STAGE_1 = 0;

    private const int STAGE_2 = 300;

    private const int STAGE_3 = 750;

    private const int STAGE_4 = 1400;

    private const int STAGE_5 = 2850;

    private const int STAGE_6 = 4100;

    private const int STAGE_7 = 5500;

    private const int STAGE_8 = 6900;

    private const int STAGE_9 = 8500;

    public int arraycounter;

    public ArrayList controlArray = new ArrayList();

    private bool destringAloneBall;

    public bool dropingDown;

    public float dropDownTime;

    public bool isPaused;

    public bool noSound;

    public bool gameOver;

    public bool arcadeMode;

    public float bottomBorder;

    public float topBorder;

    public float leftBorder;

    public float rightBorder;

    public float gameOverBorder;

    public float ArcadedropDownTime;

    public bool hd;

    public GameObject Fade;

    public int highScore;

    public AudioClip pops;

    public AudioClip click;

    public AudioClip levelBells;

    private float appearLevelTime;

    public GameObject boxCatapult;

    public GameObject boxFirst;

    public GameObject boxSecond;

    public GameObject ElectricLiana;

    public GameObject BonusLiana;

    public GameObject BonusScore;

    public static bool ElectricBoost;

    private bool BonusLianaCounter;

    private bool gameOverShown;

    public static bool StopControl;

    public GameObject finger;

    public GameObject BoostChanging;

    public creatorBall creatorBall;

    public GameObject GameOverBorderObject;

    public GameObject TopBorder;

    public Transform Balls;

    public Hashtable animTable = new Hashtable();

    public static Vector3 lastBall;

    public GameObject FireEffect;

    public static int doubleScore = 1;

    public int TotalTargets;

    public int countOfPreparedToDestroy;

    public int bugSounds;

    public int potSounds;

    public GameObject ExitPanel;

    public GameObject Layer;

    public static Dictionary<int, BallColor> colorsDict = new Dictionary<int, BallColor>();

    private int _ComboCount;

    public GameObject[] DoubleBonus;

    private bool onetimeEntry = true;

    public bool BallChanged = true;

    public bool BallReady = true;

    public GameObject popupScore;

    private int TargetCounter;

    public GameObject[] starsObject;

    public int stars;

    public GameObject perfect;

    public GameObject[] boosts;

    public GameObject[] locksBoosts;

    public GameObject arrows;

    private int stageTemp;

    public GameObject newBall2;

    private int maxCols;

    private int maxRows;

    private LIMIT limitType;

    private int limit;

    private int colorLimit;

    public GameObject Fire;

    public GameObject PauseMenu;

    public GameObject SettingsPanel;

    public GameObject BoostPanel;

    private bool PauseAnimRunning;

    public GameObject coin_targetpos;

    public GameObject diamond_targetpos;

    public GameObject fragment_targetpos;

    GameObject ball_container;

    public GameObject spi_wheel_panel;

    public Sprite plus_boost_, count_boost_;


    public float collect_diamond_value;

    //public GameObject add_bomb, add_color, add_fire, add_line;
    //public GameObject counter_bomb,counter_color,counter_;
}
