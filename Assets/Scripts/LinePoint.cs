using System;
using UnityEngine;

public class LinePoint : MonoBehaviour
{
	private void Start()
	{
		base.transform.position = DrawLine.waypoints[0];
		this.nextWayPoint++;
	}

	private void Update()
	{
		if (this.startPoint == this.nextPoint)
		{
			base.GetComponent<SpriteRenderer>().enabled = false;
		}
		this.timeLerped += Time.deltaTime;
		base.transform.position = Vector3.MoveTowards(base.transform.position, this.nextPoint, this.speed * Time.deltaTime);
		if ((Vector2)base.transform.position == this.nextPoint)
		{
			this.nextWayPoint = 0;
			base.transform.position = this.startPoint;
		}
	}

	private int nextWayPoint;

	private float timeToLerp = 5f;

	private float timeLerped;

	private float speed = 1f;

	public Vector2 startPoint;

	public Vector2 nextPoint;
}
