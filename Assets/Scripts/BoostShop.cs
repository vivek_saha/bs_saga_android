using System;
using UnityEngine;
using UnityEngine.UI;

public class BoostShop : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
	}

	public void SetBoost(BoostType _boostType)
	{
		this.boostType = _boostType;
		base.gameObject.SetActive(true);
		this.icon.sprite = this.icons[(int)_boostType];
		this.title.text = this.titles[(int)_boostType];
		//this.description.text = this.descriptions[(int)_boostType];
		this.price.text = string.Empty + this.prices[(int)_boostType];
		//this.Header.sprite = this.Headers[(int)_boostType];
	}

	public void BuyBoost()
	{
		base.GetComponent<AnimationManager>().BuyBoost(this.boostType, this.prices[(int)this.boostType]);
	}

	public void Watch_video()
    {
		base.GetComponent<AnimationManager>().BuyBoost_watch_videoo_fun(this.boostType, 2);
	}

	public Sprite[] icons;

	public string[] titles;

	public string[] descriptions;

	public Sprite[] Headers;

	public int[] prices;

	public Image icon;

	public Text title;

	public Text description;

	public Text price;

	public Image Header;

	private BoostType boostType;
}
