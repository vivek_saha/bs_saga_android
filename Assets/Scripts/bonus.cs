using System;
using System.Collections;
using UnityEngine;

public class bonus : MonoBehaviour
{
	private void Start()
	{
		base.StartCoroutine(this.flyUp());
		this.timed = Time.time;
		if (base.name.Contains("bonus_score"))
		{
			base.transform.position = new Vector3(base.transform.position.x, (float)UnityEngine.Random.Range(-2, 5), -10f);
		}
		this.revertButterFly = UnityEngine.Random.Range(0, 2);
		if (this.revertButterFly == 1)
		{
			base.transform.position = new Vector3(3f, base.transform.position.y, -10f);
			this.target = new Vector3(base.transform.position.x - 8f, base.transform.position.y, base.transform.localScale.z);
		}
		else
		{
			base.transform.position = new Vector3(-3f, base.transform.position.y, -10f);
			base.transform.localScale = new Vector3(base.transform.localScale.x * -1f, base.transform.localScale.y, base.transform.localScale.z);
			this.target = new Vector3(base.transform.position.x + 8f, base.transform.position.y, base.transform.localScale.z);
		}
	}

	private void Update()
	{
		base.transform.position = Vector3.Lerp(base.transform.position, this.target, (Time.time - this.timed) * 0.02f);
		if (base.transform.position.x < -4f || base.transform.position.x > 4f)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private IEnumerator flyUp()
	{
		yield return new WaitForSeconds(0.2f);
		yield break;
	}

	public void OnTriggerEnter2D(Collider2D owner)
	{
		if (owner.gameObject.name.IndexOf("ball") == 0 && owner.gameObject.GetComponent<ball>().setTarget)
		{
			if (base.name.Contains("bonus_liana"))
			{
				if (!mainscript.ElectricBoost)
				{
					mainscript.Instance.SwitchLianaBoost();
				}
			}
			else if (base.name.Contains("bonus_score"))
			{
			}
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public int revertButterFly;

	private Vector3 target;

	private float timed;
}
