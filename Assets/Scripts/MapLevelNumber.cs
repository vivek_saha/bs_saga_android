using System;
using UnityEngine;

public class MapLevelNumber : MonoBehaviour
{
	private void Start()
	{
		base.GetComponent<Renderer>().sortingLayerName = "New Layer 1";
		base.GetComponent<Renderer>().sortingOrder = 1;
		int num = int.Parse(base.transform.parent.name.Replace("Level", string.Empty));
		base.GetComponent<TextMesh>().text = string.Empty + num;
		if (num >= 10)
		{
			base.transform.position += Vector3.left * 0.05f;
		}
		if (num == 1 || num == 11)
		{
			base.transform.position += Vector3.right * 0.05f;
		}
	}

	private void Update()
	{
	}
}
