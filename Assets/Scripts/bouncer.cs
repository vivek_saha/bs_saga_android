using System;
using System.Collections;
using UnityEngine;

public class bouncer : MonoBehaviour
{
	private void Start()
	{
		this.isPaused = Camera.main.GetComponent<mainscript>().isPaused;
		this.gameOverBorder = Camera.main.GetComponent<mainscript>().gameOverBorder;
		this.targetPrepare = base.transform.position;
	}

	private void Update()
	{
	}

	private IEnumerator bonceCoroutine()
	{
		while (Vector3.Distance(base.transform.position, this.targetPrepare) > 1f && !this.isPaused && !base.GetComponent<ball>().setTarget)
		{
			base.transform.position = Vector3.Lerp(this.tempPosition, this.targetPrepare, (Time.time - this.startTime) * 2f);
			yield return new WaitForSeconds(0.0333333351f);
		}
		yield break;
	}

	private IEnumerator bonceToCatapultCoroutine()
	{
		base.Invoke("delayedBonceToCatapultCoroutine", 0.5f);
		yield return new WaitForSeconds(0.2f);
		yield break;
	}

	private void delayedBonceToCatapultCoroutine()
	{
		base.transform.position = this.targetPrepare;
		base.GetComponent<ball>().newBall = true;
	}

	private void newBall()
	{
		base.GetComponent<ball>().newBall = true;
		Grid.waitForAnim = false;
	}

	public void bounceToCatapult(Vector3 vector3)
	{
		vector3 = new Vector3(vector3.x, vector3.y, base.gameObject.transform.position.z);
		this.tempPosition = base.transform.position;
		this.targetPrepare = vector3;
		this.startBounce = true;
		this.startTime = Time.time;
		iTween.MoveTo(base.gameObject, iTween.Hash(new object[]
		{
			"position",
			vector3,
			"time",
			0.3,
			"easetype",
			iTween.EaseType.linear,
			"onComplete",
			"newBall"
		}));
		Grid.waitForAnim = false;
	}

	public void bounceTo(Vector3 vector3)
	{
		vector3 = new Vector3(vector3.x, vector3.y, base.gameObject.transform.position.z);
		this.tempPosition = base.transform.position;
		this.targetPrepare = vector3;
		this.startBounce = true;
		this.startTime = Time.time;
		if (GamePlay.Instance.GameStatus == GameState.Playing)
		{
			iTween.MoveTo(base.gameObject, iTween.Hash(new object[]
			{
				"position",
				vector3,
				"time",
				0.3,
				"easetype",
				iTween.EaseType.linear
			}));
		}
		else if (GamePlay.Instance.GameStatus == GameState.Win)
		{
			iTween.MoveTo(base.gameObject, iTween.Hash(new object[]
			{
				"position",
				vector3,
				"time",
				1E-05,
				"easetype",
				iTween.EaseType.linear
			}));
		}
	}

	public void dropDown()
	{
		int layerMask = 1 << LayerMask.NameToLayer("Mesh");
		Collider2D[] array = Physics2D.OverlapCircleAll(base.transform.position, 0.5f, layerMask);
		foreach (Collider2D collider2D in array)
		{
			float num = Vector3.Distance(new Vector3(base.transform.position.x - this.offset, base.transform.position.y, base.transform.position.z), collider2D.transform.position);
			if (num <= 0.9f && collider2D.transform.position.y + 0.1f < base.transform.position.y)
			{
				Vector3 vector;
				if (collider2D.GetComponent<Grid>().offset > 0f)
				{
					vector = new Vector3(base.transform.position.x + collider2D.GetComponent<Grid>().offset, collider2D.transform.position.y, base.transform.position.z);
				}
				else
				{
					vector = new Vector3(collider2D.transform.position.x, collider2D.transform.position.y, base.transform.position.z);
				}
				this.bounceTo(vector);
				return;
			}
		}
	}

	public bool checkNearestBall(ArrayList b)
	{
		if (base.transform.position.y >= 0.828125f * Camera.main.orthographicSize)
		{
			Camera.main.GetComponent<mainscript>().controlArray = this.addFrom(b, Camera.main.GetComponent<mainscript>().controlArray);
			b.Clear();
			return true;
		}
		if (this.findInArray(Camera.main.GetComponent<mainscript>().controlArray, base.gameObject))
		{
			b.Clear();
			return true;
		}
		b.Add(base.gameObject);
		IEnumerator enumerator = this.nearBalls.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				GameObject gameObject = (GameObject)obj;
				if (gameObject.gameObject.layer == 9 && gameObject != base.gameObject)
				{
					float num = Vector3.Distance(base.transform.position, gameObject.transform.position);
					if (num <= 0.8f && num > 0f && !this.findInArray(b, gameObject.gameObject))
					{
						Camera.main.GetComponent<mainscript>().arraycounter++;
						if (gameObject.GetComponent<bouncer>().checkNearestBall(b))
						{
							return true;
						}
					}
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return false;
	}

	public bool findInArray(ArrayList b, GameObject destObj)
	{
		IEnumerator enumerator = b.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				GameObject x = (GameObject)obj;
				if (x == destObj)
				{
					return true;
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return false;
	}

	public ArrayList addFrom(ArrayList b, ArrayList b2)
	{
		IEnumerator enumerator = b.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				GameObject gameObject = (GameObject)obj;
				if (!this.findInArray(b2, gameObject))
				{
					b2.Add(gameObject);
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return b2;
	}

	public void connectNearBalls()
	{
		int layerMask = 1 << LayerMask.NameToLayer("Ball");
		Collider2D[] array = Physics2D.OverlapCircleAll(base.transform.position, 0.5f, layerMask);
		this.nearBalls.Clear();
		foreach (Collider2D collider2D in array)
		{
			if (this.nearBalls.Count <= 7)
			{
				this.nearBalls.Add(collider2D.gameObject);
			}
		}
		this.countNEarBalls = this.nearBalls.Count;
	}

	public void checkNextNearestColor(ArrayList b, int counter)
	{
		Vector3 localScale = base.transform.localScale;
		int layerMask = 1 << LayerMask.NameToLayer("Ball");
		Collider2D[] array = Physics2D.OverlapCircleAll(base.transform.position, 1f, layerMask);
		foreach (Collider2D collider2D in array)
		{
			if (collider2D.gameObject.tag == base.tag)
			{
				GameObject gameObject = collider2D.gameObject;
				float num = Vector3.Distance(base.transform.position, gameObject.transform.position);
				if (num <= 1f && !this.findInArray(b, gameObject))
				{
					counter++;
					b.Add(gameObject);
					gameObject.GetComponent<ball>().checkNextNearestColor(b, counter);
				}
			}
		}
	}

	private Vector3 tempPosition;

	private Vector3 targetPrepare;

	private bool isPaused;

	public bool startBounce;

	private float startTime;

	public float offset;

	public ArrayList nearBalls = new ArrayList();

	private GameObject Meshes;

	public int countNEarBalls;

	private float gameOverBorder;
}
