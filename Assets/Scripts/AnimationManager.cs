using InitScriptName;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationManager : MonoBehaviour
{
    public static AnimationManager instance { get; set; }

    private void OnEnable()
    {
        AnimationManager.instance = this;
        if (!this.PlayOnEnable || base.name == "Fire")
        {
            //Debug.Log("damn fire");
        }
        if (base.name == "MenuPlay")
        {
            for (int i = 1; i <= 3; i++)
            {
                base.transform.Find("Image").Find("Star" + i).gameObject.SetActive(false);
            }
            int @int = PlayerPrefs.GetInt(string.Format("Level.{0:000}.StarsCount", PlayerPrefs.GetInt("OpenLevel")), 0);
            if (@int > 0)
            {
                for (int j = 1; j <= @int; j++)
                {
                    base.transform.Find("Image").Find("Star" + j).gameObject.SetActive(true);
                }
            }
            else
            {
                for (int k = 1; k <= 3; k++)
                {
                    base.transform.Find("Image").Find("Star" + k).gameObject.SetActive(false);
                }
            }
        }
        if (base.name == "SettingButton" || base.name == "MenuPause" || base.name == "Option")
        {
            if (PlayerPrefs.GetInt("music", 1) == 1)
            {
                MonoBehaviour.print("musicon");
                GameObject.Find("Music").GetComponent<AudioSource>().volume = 0.4f;
                //this.music.GetComponent<Image>().sprite = this.musicon;
                Music_lock.SetActive(false);
            }
            else
            {
                MonoBehaviour.print("musicoff");
                GameObject.Find("Music").GetComponent<AudioSource>().volume = 0f;
                //this.music.GetComponent<Image>().sprite = this.musicoff;
                Music_lock.SetActive(true);
            }
            if (PlayerPrefs.GetInt("sound", 1) == 1)
            {
                MonoBehaviour.print("soundon");
                SoundBase.Instance.GetComponent<AudioSource>().volume = 1f;
                //this.sound.GetComponent<Image>().sprite = this.soundon;
                Sound_lock.SetActive(false);
            }
            else
            {
                MonoBehaviour.print("sound0ff");
                SoundBase.Instance.GetComponent<AudioSource>().volume = 0f;
                //this.sound.GetComponent<Image>().sprite = this.sound0ff;
                Sound_lock.SetActive(true);
            }
        }
    }

    private void OnDisable()
    {
    }

    public void OnFinished()
    {
        if (base.name == "MenuComplete")
        {
            base.StartCoroutine(this.MenuComplete());
            base.StartCoroutine(this.MenuCompleteScoring());
        }
        if (base.name == "MenuPlay")
        {
            InitScript.Instance.currentTarget = LevelData.GetTarget(PlayerPrefs.GetInt("OpenLevel"));
        }
    }

    private IEnumerator MenuComplete()
    {
        PlayerPrefs.SetInt("LevelStar" + PlayerPrefs.GetInt("OpenLevel"), mainscript.Instance.stars);
        yield return new WaitForSeconds(0.5f);
        for (int i = 1; i <= mainscript.Instance.stars; i++)
        {
            base.transform.Find("Image").Find("Star" + i).gameObject.SetActive(true);
            yield return new WaitForSeconds(0.3f);
            SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.GetStarPlaying);
            yield return new WaitForSeconds(0.5f);
        }
        yield break;
    }

    private IEnumerator MenuCompleteScoring()
    {
        Text scores = base.transform.Find("Image").Find("Score").GetComponent<Text>();
        for (int i = 0; i <= mainscript.Score; i += 500)
        {
            scores.text = "Score : " + i;
            yield return new WaitForSeconds(1E-05f);
        }
        scores.text = "Score : " + mainscript.Score;
        yield break;
    }

    public void PlaySoundButton()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
    }

    public IEnumerator Close()
    {
        yield return new WaitForSeconds(0.5f);
        yield break;
    }

    public void CloseMenu()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (base.gameObject.name == "MenuPreGameOver")
        {
            //if (!base.gameObject.transform.GetChild(0).transform.Find("ReleasePanel").gameObject.activeSelf)
            //{
            //    base.gameObject.transform.GetChild(0).transform.Find("ReleasePanel").gameObject.SetActive(true);
            //    MonoBehaviour.print("======>" + mainscript.Instance.TotalTargets);
            //    MonoBehaviour.print("======>" + mainscript.Instance.TargetCounter1);
            //    base.gameObject.transform.GetChild(0).transform.Find("ReleasePanel").GetChild(0).GetComponent<Text>().text = "You are so Close! \n Only " + (mainscript.Instance.TotalTargets - mainscript.Instance.TargetCounter1) + " more Puppy to Release!";
            //    base.gameObject.transform.GetChild(0).transform.Find("BlackTransparent").gameObject.SetActive(true);
            //}
            //else
            //{
            this.ShowGameOver();
            //}
        }
        if (base.gameObject.name == "CongratulationPanel")
        {
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        if (base.gameObject.name == "MenuComplete")
        {
            if (PlayerPrefs.GetInt("OpenLevel") != 100)
            {
                LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
                base.StartCoroutine(this.LoadNewScene("map"));
                PlayerPrefs.SetInt("OpenLevel", PlayerPrefs.GetInt("OpenLevel") + 1);
                InitScript.Instance.AddDiamonds(mainscript.Instance.collect_diamond_value);
                PlayerPrefs.Save();
                Update_API.Instance.Update_data();
                MonoBehaviour.print(string.Concat(new object[]
                {
                    "Max level=======>",
                    PlayerPrefs.GetInt("MaxLevel"),
                    "<========OpenLevels======>",
                    PlayerPrefs.GetInt("OpenLevel")
                }));
                if (PlayerPrefs.GetInt("MaxLevel", 0) < PlayerPrefs.GetInt("OpenLevel"))
                {
                    PlayerPrefs.SetString("LevelPass", "Yes");
                    PlayerPrefs.SetInt("MaxLevel", PlayerPrefs.GetInt("OpenLevel"));
                    MonoBehaviour.print("Max level=======>" + PlayerPrefs.GetInt("MaxLevel"));
                }
                //GoogleMobileAdsDemoScript.instance.ShowInterstitial();
            }
            else
            {
                base.gameObject.SetActive(false);
                //GameObject.Find("Canvas").transform.Find("CongratulationPanel").gameObject.SetActive(true);
            }
        }
        if (base.gameObject.name == "PauseMenu")
        {
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        if (base.gameObject.name == "MenuGameOver")
        {
            //GoogleMobileAdsDemoScript.instance.ShowInterstitial();
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "game" && GamePlay.Instance.GameStatus == GameState.Pause)
        {
            GamePlay.Instance.GameStatus = GameState.WaitAfterClose;
        }
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Settings")
        {
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.swish[1]);
    }

    public void SetFalseObject(GameObject game)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        game.SetActive(false);
    }

    public void SetObject(GameObject game)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        game.SetActive(true);
    }

    public void Play()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (base.gameObject.name == "MenuPreGameOver")
        {
            if (GamePlay.GamePreover == 1)
            {
                //if (InitScript.Gems >= 5)
                //{
                    //InitScript.Instance.SpendGems(5);
                    LevelData.LimitAmount += 5f;
                    GamePlay.Instance.GameStatus = GameState.WaitAfterClose;
                    base.gameObject.SetActive(false);
                    //GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().enabled = true;
                    //GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAniOut", -1, 0f);
                    //MonoBehaviour.print("===========+>");
                    mainscript.Instance.Layer.SetActive(true);
                    mainscript.Instance.Set_false_layer_Function();
                    //GameObject.Find("CanvasMoves").transform.Find("GameObject").GetComponent<Button>().interactable = true;
                //}
                //else
                //{
                //    MonoBehaviour.print("==-----else---===>");
                //    this.BuyGemsGameScene();
                //}
            }
            
        }
        else if (base.gameObject.name == "MenuGameOver")
        {
            //GoogleMobileAdsDemoScript.instance.ShowInterstitial();
            PlayerPrefs.Save();
            Update_API.Instance.Update_data();
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        else if (base.gameObject.name == "MenuPlay")
        {
            //if (InitScript.Lifes > 0)
            //{
            //InitScript.Instance.SpendLife(1);
            LoadingPanelAnim.instance.LoadingPanelAnimIN("game");
            base.StartCoroutine(this.LoadNewScene("game"));
            Ads_priority_script.Instance.Show_interrestial();
            //}
            //else
            //{
            //    this.BuyLifeShop();
            //}
        }
        else if (base.gameObject.name == "PlayMain")
        {
            PlayerPrefs.SetString("Scene", "menu");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
    }

    public void On_click_watch_video()
    {
        Ads_priority_script.Instance.watch_video(Ads_reward_type.GameoverPre);
    }
    public void Watch_Video_reward()
    {
        if (GamePlay.GamePreover == 1)
        {
            LevelData.LimitAmount += 5f;
            Gameplay_panel_script.Instance.chaaracter_anim.GetComponent<Animator>().Play("charcater_idel");
        }
        

        GamePlay.Instance.GameStatus = GameState.WaitAfterClose;
        base.gameObject.SetActive(false);
        //GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().enabled = true;
        //GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAniOut", -1, 0f);
        MonoBehaviour.print("===========+>");
        mainscript.Instance.Layer.SetActive(true);
        mainscript.Instance.Set_false_layer_Function();
        //GameObject.Find("CanvasMoves").transform.Find("GameObject").GetComponent<Button>().interactable = true;
    }
    private IEnumerator LoadNewScene(string Scenename)
    {
        yield return new WaitForSeconds(1f);
        Application.LoadLevelAsync(Scenename);
        yield break;
    }

    public void PlayTutorial()
    {
        GamePlay.Instance.GameStatus = GameState.Playing;
    }

    public void Next()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        //StartCoroutine("level_finish");
       
        //InitScript.Instance.AddDiamonds()
        this.CloseMenu();
    }


    IEnumerator level_finish()
    {
        yield return new WaitForSecondsRealtime(2f);
       
    }
    public void BuyGems()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        CanvasManager.instance.Setobjects(gemshop_script.Instance.gemshopp);
    }

    public void Buy(GameObject pack)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (pack.name == "Pack1")
        {
            InitScript.waitedPurchaseGems = int.Parse(pack.transform.Find("Count").GetComponent<Text>().text.Replace("x ", string.Empty));
        }
        if (pack.name == "Pack2")
        {
            InitScript.waitedPurchaseGems = int.Parse(pack.transform.Find("Count").GetComponent<Text>().text.Replace("x ", string.Empty));
        }
        if (pack.name == "Pack3")
        {
            InitScript.waitedPurchaseGems = int.Parse(pack.transform.Find("Count").GetComponent<Text>().text.Replace("x ", string.Empty));
        }
        if (pack.name == "Pack4")
        {
            InitScript.waitedPurchaseGems = int.Parse(pack.transform.Find("Count").GetComponent<Text>().text.Replace("x ", string.Empty));
        }
        this.CloseMenu();
    }

    public void BuyLifeShop()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Lifes < InitScript.CapOfLife)
        {
            CanvasManager.instance.Setobjects(GameObject.Find("Canvas").transform.Find("LiveShop").gameObject);
        }
    }

    public void BuyLife(GameObject button)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Gems >= int.Parse(button.transform.Find("Price").GetComponent<Text>().text))
        {
            InitScript.Instance.SpendGems(int.Parse(button.transform.Find("Price").GetComponent<Text>().text));
            InitScript.Instance.RestoreLifes();
            GameObject.Find("Canvas").transform.Find("LiveShop").gameObject.SetActive(false);
            this.CloseMenu();
        }
        else
        {
            CanvasManager.instance.Setobjects(gemshop_script.Instance.gemshopp);
        }
    }

    public void ShowGameOver()
    {
        SoundBase.Instance.GetComponent<AudioSource>().Play();
        GameObject.Find("Canvas").transform.Find("MenuGameOver").gameObject.SetActive(true);
        //GameObject.Find("CanvasMoves").transform.Find("GameObject").GetComponent<Button>().interactable = false;
        base.gameObject.SetActive(false);
    }

    public void ShowSettings(GameObject menuSettings)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (!menuSettings.activeSelf)
        {
            if (PlayerPrefs.GetInt("music", 1) == 1)
            {
                MonoBehaviour.print("musicon");
                //this.music.GetComponent<Image>().sprite = this.musicon;
                Music_lock.SetActive(false);
            }
            else
            {
                MonoBehaviour.print("musicoff");
                //this.music.GetComponent<Image>().sprite = this.musicoff;
                Music_lock.SetActive(true);
            }
            if (PlayerPrefs.GetInt("sound", 1) == 1)
            {
                MonoBehaviour.print("soundon");
                //this.sound.GetComponent<Image>().sprite = this.soundon;
                Sound_lock.SetActive(false);
            }
            else
            {
                MonoBehaviour.print("sound0ff");
                //this.sound.GetComponent<Image>().sprite = this.sound0ff;
                Sound_lock.SetActive(true);
            }
            menuSettings.SetActive(true);
        }
        else
        {
            menuSettings.SetActive(false);
        }
    }

    public void SoundOff()
    {
        if (PlayerPrefs.GetInt("sound", 0) == 1)
        {
            PlayerPrefs.SetInt("sound", 0);
            SoundBase.Instance.GetComponent<AudioSource>().volume = 0f;
            //this.sound.GetComponent<Image>().sprite = this.sound0ff;
            Sound_lock.SetActive(true);
            MonoBehaviour.print("SoundOff===>0");
        }
        else
        {
            MonoBehaviour.print("SoundOff===>1");
            PlayerPrefs.SetInt("sound", 1);
            Sound_lock.SetActive(false);
            //this.sound.GetComponent<Image>().sprite = this.soundon;
            SoundBase.Instance.GetComponent<AudioSource>().volume = 1f;
        }
        PlayerPrefs.SetInt("Sound", (int)SoundBase.Instance.GetComponent<AudioSource>().volume);
        PlayerPrefs.Save();
    }




    public void MusicOff()
    {
        if (PlayerPrefs.GetInt("music", 0) == 1)
        {
            PlayerPrefs.SetInt("music", 0);
            //this.music.GetComponent<Image>().sprite = this.musicoff;
            Music_lock.SetActive(true);
            GameObject.Find("Music").GetComponent<AudioSource>().volume = 0f;
        }
        else
        {
            PlayerPrefs.SetInt("music", 1);
            //this.music.GetComponent<Image>().sprite = this.musicon;
            Music_lock.SetActive(false);
            GameObject.Find("Music").GetComponent<AudioSource>().volume = 0.4f;
        }
        PlayerPrefs.SetInt("Music", (int)GameObject.Find("Music").GetComponent<AudioSource>().volume);
        PlayerPrefs.Save();
    }

    public void VibrateOff()
    {
        if (PlayerPrefs.GetInt("vibrate", 0) == 1)
        {
            PlayerPrefs.SetInt("vibrate", 0);
            //SoundBase.Instance.GetComponent<AudioSource>().volume = 0f;
            //this.sound.GetComponent<Image>().sprite = this.sound0ff;
            vibrate_lock.SetActive(true);
            MonoBehaviour.print("vibrateOff===>0");
        }
        else
        {
            MonoBehaviour.print("vibrateOff===>1");
            PlayerPrefs.SetInt("vibrate", 1);
            vibrate_lock.SetActive(false);
            //this.sound.GetComponent<Image>().sprite = this.soundon;
            //SoundBase.Instance.GetComponent<AudioSource>().volume = 1f;
        }
        //PlayerPrefs.SetInt("Sound", (int)SoundBase.Instance.GetComponent<AudioSource>().volume);
        PlayerPrefs.Save();
    }

    public void Info()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "map" || UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "menu")
        {
            GameObject.Find("Canvas").transform.Find("Tutorial").gameObject.SetActive(true);
        }
        else
        {
            GameObject.Find("Canvas").transform.Find("PreTutorial").gameObject.SetActive(true);
        }
    }

    public void Quit()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "game")
        {
            Ads_priority_script.Instance.Show_interrestial();
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        else
        {
            Application.Quit();
        }
    }

    public void FiveBallsBoost()
    {
        if (GamePlay.Instance.GameStatus != GameState.Playing)
        {
            return;
        }
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        //if (InitScript.Instance.FiveBallsBoost > 0)
        //{
        //if (GamePlay.Instance.GameStatus == GameState.Playing)
        //{
        //    InitScript.Instance.SpendBoost(BoostType.FiveBallsBoost);
        //    //mainscript.Instance.PauseButtonPopOut(GameObject.Find("PauseMenu"));
        //}
        //}
        //else
        //{
        //    this.OpenBoostShop(BoostType.FiveBallsBoost);
        //}
    }

    public void ColorBallBoost()
    {
        //if (GamePlay.Instance.GameStatus != GameState.Playing)
        //{
        //    return;
        //}
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Instance.ColorBallBoost > 0)
        {
            if (GamePlay.Instance.GameStatus == GameState.Playing)
            {
                InitScript.Instance.SpendBoost(BoostType.ColorBallBoost);
                //mainscript.Instance.PauseButtonPopOut(GameObject.Find("PauseMenu"));
            }
        }
        else
        {
            this.OpenBoostShop(BoostType.ColorBallBoost);
        }
    }


    public void LineBallBoost()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Instance.LineBallBoost > 0)
        {
            if (GamePlay.Instance.GameStatus == GameState.Playing)
            {
                InitScript.Instance.SpendBoost(BoostType.LineBallBoost);
                //mainscript.Instance.PauseButtonPopOut(GameObject.Find("PauseMenu"));
            }
        }
        else
        {
            this.OpenBoostShop(BoostType.LineBallBoost);
        }
    }

    public void BombBallBoost()
    {
        //if (GamePlay.Instance.GameStatus != GameState.Playing)
        //{
        //    return;
        //}
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Instance.BombBallBoost > 0)
        {
            if (GamePlay.Instance.GameStatus == GameState.Playing)
            {
                InitScript.Instance.SpendBoost(BoostType.BombBallBoost);
                //mainscript.Instance.PauseButtonPopOut(GameObject.Find("PauseMenu"));
            }
        }
        else
        {
            this.OpenBoostShop(BoostType.BombBallBoost);
        }
    }


    public void FireBallBoost()
    {
        //if (GamePlay.Instance.GameStatus != GameState.Playing)
        //{
        //    return;
        //}
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Instance.FireBallBoost > 0)
        {
            if (GamePlay.Instance.GameStatus == GameState.Playing)
            {
                InitScript.Instance.SpendBoost(BoostType.FireBallBoost);
                //mainscript.Instance.PauseButtonPopOut(GameObject.Find("PauseMenu"));
            }
        }
        else
        {
            this.OpenBoostShop(BoostType.FireBallBoost);
        }
    }

    public void OpenBoostShop(BoostType boosType)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        GameObject.Find("Canvas").transform.Find("BoostShop").gameObject.GetComponent<BoostShop>().SetBoost(boosType);
        switch (boosType)
        {
            case BoostType.ColorBallBoost:
                GameObject.Find("Canvas").transform.Find("BoostShop").gameObject.transform.Find("Title").GetComponent<Text>().text= "Color Ball";
                break;
            case BoostType.FireBallBoost:
                GameObject.Find("Canvas").transform.Find("BoostShop").gameObject.transform.Find("Title").GetComponent<Text>().text = "Fire Ball";
                break;
            case BoostType.LineBallBoost:
                GameObject.Find("Canvas").transform.Find("BoostShop").gameObject.transform.Find("Title").GetComponent<Text>().text = "Line Ball";
                break;
            case BoostType.BombBallBoost:
                GameObject.Find("Canvas").transform.Find("BoostShop").gameObject.transform.Find("Title").GetComponent<Text>().text = "Bomb Ball";
                break;
            default:
                break;
        }
    }

    public void BuyBoost(BoostType boostType, int price)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Gems >= price)
        {
            InitScript.Instance.BuyBoost(boostType, 1, price);
            this.CloseMenu();
        }
        else
        {
            GameManager.Instance.nem_popup_fun("Not Enough Coins!!!");
            //this.BuyGemsGameScene();

        }
    }


    public void BuyBoost_watch_videoo_fun(BoostType boostType, int price)
    {
        Ads_priority_script.Instance.Buy_boost_watch_video(boostType, price);
    }

    public void BuyGemsGameScene()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        gemshop_script.Instance.gemshopp.SetActive(true);
    }


    public void Contact_mail()
    {

        string email = "sahajanandinfotech1@gmail.com";
        string subject = MyEscapeURL("");
        string body = MyEscapeURL("");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }
    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public GameObject Loading_Panel;

    public bool PlayOnEnable = true;

    private bool WaitForPickupFriends;

    private bool WaitForAksFriends;

    private Dictionary<string, string> parameters;

    public GameObject sound;

    public GameObject music;

    //public Sprite musicoff;

    //public Sprite musicon;

    public GameObject Music_lock;

    public GameObject Sound_lock;

    public GameObject vibrate_lock;
}
