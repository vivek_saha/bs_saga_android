using System;
using System.Collections;
using System.Collections.Generic;
using InitScriptName;
using UnityEngine;
using UnityEngine.UI;

public class SpinWheel : MonoBehaviour
{
	public static SpinWheel instance { get; set; }

	private void Start()
	{
		SpinWheel.instance = this;
		this.spinning = false;
		this.anglePerItem = (float)(360 / this.prize.Count);
		base.StartCoroutine(UniLocalNotificationSample.instance.ScheduleDailyReward());
	}

	private void Update()
	{
	}

	public void clickFreeSprial()
	{
		this.CloseButton.SetActive(false);
		if (this.SpinText.transform.GetChild(1).gameObject.activeSelf)
		{
			if (InitScript.Gems > 0)
			{
				this.SpinText.SetActive(false);
				InitScript.Gems--;
				this.randomTime = UnityEngine.Random.Range(1, 4);
				this.itemNumber = UnityEngine.Random.Range(0, this.prize.Count);
				float maxAngle = (float)(720 * this.randomTime) + (float)this.itemNumber * this.anglePerItem;
				base.StartCoroutine(this.SpinTheWheel((float)(5 * this.randomTime), maxAngle));
				this.SpinText.transform.GetChild(0).gameObject.SetActive(false);
				this.SpinText.transform.GetChild(1).gameObject.SetActive(true);
			}
			else
			{
				GameObject.Find("Canvas").transform.Find("FreeSpinsPanel").gameObject.SetActive(false);
				AnimationManager.instance.BuyGems();
				MonoBehaviour.print("--------->");
			}
		}
		else
		{
			this.SpinText.SetActive(false);
			base.StartCoroutine(UniLocalNotificationSample.instance.ScheduleDailyReward());
			UnityEngine.Object.FindObjectOfType<RewardGettingTimer>().endtime = DateTime.Now;
			this.randomTime = UnityEngine.Random.Range(1, 4);
			this.itemNumber = UnityEngine.Random.Range(0, this.prize.Count);
			float maxAngle2 = (float)(720 * this.randomTime) + (float)this.itemNumber * this.anglePerItem;
			base.StartCoroutine(this.SpinTheWheel((float)(5 * this.randomTime), maxAngle2));
			this.SpinText.transform.GetChild(0).gameObject.SetActive(false);
			this.SpinText.transform.GetChild(1).gameObject.SetActive(true);
			if (PlayerPrefs.GetInt("SpinUsed") == 0)
			{
				PlayerPrefs.SetInt("SpinUsed", 1);
				UnityEngine.Object.FindObjectOfType<RewardGettingTimer>().StarTimer();
				PlayerPrefs.SetInt("Starting_Year", UnityEngine.Object.FindObjectOfType<RewardGettingTimer>().endtime.Year);
				PlayerPrefs.SetInt("Starting_Month", UnityEngine.Object.FindObjectOfType<RewardGettingTimer>().endtime.Month);
				PlayerPrefs.SetInt("Starting_Day", UnityEngine.Object.FindObjectOfType<RewardGettingTimer>().endtime.Day);
				PlayerPrefs.SetInt("Starting_Hour", UnityEngine.Object.FindObjectOfType<RewardGettingTimer>().endtime.Hour);
				PlayerPrefs.SetInt("Starting_Minute", UnityEngine.Object.FindObjectOfType<RewardGettingTimer>().endtime.Minute);
				PlayerPrefs.SetInt("Starting_Second", UnityEngine.Object.FindObjectOfType<RewardGettingTimer>().endtime.Second);
				UnityEngine.Object.FindObjectOfType<RewardGettingTimer>().Map_but.GetComponent<Button>().interactable = false;
				UnityEngine.Object.FindObjectOfType<RewardGettingTimer>().Spintext.GetComponent<Animator>().enabled = false;

            }
		}
	}

	private IEnumerator SpinTheWheel(float time, float maxAngle)
	{
		this.spinning = true;
		float timer = 0f;
		float startAngle = base.transform.eulerAngles.z;
		maxAngle -= startAngle;
		int animationCurveNumber = UnityEngine.Random.Range(0, this.animationCurves.Count);
		UnityEngine.Debug.Log("Animation Curve No. : " + animationCurveNumber);
		while (timer < time)
		{
			float angle = maxAngle * this.animationCurves[animationCurveNumber].Evaluate(timer / time);
			base.transform.eulerAngles = new Vector3(0f, 0f, angle + startAngle);
			timer += Time.deltaTime;
			yield return 0;
		}
		base.transform.eulerAngles = new Vector3(0f, 0f, maxAngle + startAngle);
		this.spinning = false;
		yield return new WaitForSeconds(1f);
		this.CloseButton.SetActive(true);
		this.GotPrizePanel.transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = this.Prizes[this.itemNumber];
		if (this.prize[this.itemNumber] == "free spin")
		{
			this.SpinText.SetActive(true);
			this.SpinText.transform.GetChild(0).gameObject.SetActive(true);
			this.SpinText.transform.GetChild(1).gameObject.SetActive(false);
		}
		else
		{
			GameObject.Find("Canvas").transform.Find("FreeSpinsPanel").GetComponent<Animator>().Play("FreeSpinPanelOut");
			this.GotPrizePanel.gameObject.SetActive(true);
			yield return new WaitForSeconds(0.5f);
			GameObject.Find("Canvas").transform.Find("FreeSpinsPanel").gameObject.SetActive(false);
			Ads_priority_script.Instance.Show_RewardedAds();
			Debug.Log(this.prize[this.itemNumber]);
			if (this.prize[this.itemNumber] == "colorball")
			{
				InitScript.Instance.BuyBoost(BoostType.ColorBallBoost, 1, 0);
			}
			else if (this.prize[this.itemNumber] == "6 coin")
			{
				InitScript.Instance.AddGems(6);
				Update_API.Instance.type = "Spin_wheel";
				Update_API.Instance.Update_data();
			}
			else if (this.prize[this.itemNumber] == "Fireball")
			{
				InitScript.Instance.BuyBoost(BoostType.FireBallBoost, 1, 0);
			}
			else if (this.prize[this.itemNumber] == "10 coin")
			{
				InitScript.Instance.AddGems(10);
				Update_API.Instance.type = "Spin_wheel";
				Update_API.Instance.Update_data();
			}
			else if (this.prize[this.itemNumber] == "ExtraLife")
			{
				InitScript.Instance.AddLife(1);
			}
			//else if (this.prize[this.itemNumber] == "5 move")
			//{
			//	InitScript.Instance.BuyBoost(BoostType.FiveBallsBoost, 1, 0);
			//}
			else if (this.prize[this.itemNumber] == "3 coin")
			{
				InitScript.Instance.AddGems(3);
				Update_API.Instance.type = "Spin_wheel";
				Update_API.Instance.Update_data();
			}
		}
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.GotDailyReward);
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.aplauds);
		yield break;
	}

	public List<string> prize;

	public List<AnimationCurve> animationCurves;

	[HideInInspector]
	public bool spinning;

	private float anglePerItem;

	private int randomTime;

	private int itemNumber;

	public GameObject GotPrizePanel;

	public Sprite[] Prizes;

	[Space(10f)]
	public GameObject SpinText;

	public GameObject CloseButton;
}
