using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingPanelAnim : MonoBehaviour
{
	public static LoadingPanelAnim instance { get; set; }

	private void Awake()
	{
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		LoadingPanelAnim.instance = this;
	}

	private void Update()
	{
	}

	public void LoadingPanelAnimIN(string SceneName)
	{
		if (this.LoadingPanel == null)
		{
			this.LoadingPanel = GameObject.Find("LoadingPanel").gameObject;
		}
		this.LoadingPanel.SetActive(true);
		this.LoadingPanel.GetComponent<Animator>().Play("LoadingPanelAnim", -1, 0f);
		this.lastRoutine = base.StartCoroutine("LoadingPanelAnimOutPanel", SceneName);
	}

	public void LoadingPanelAnimOut()
	{
		this.LoadingPanel.GetComponent<Animator>().Play("LoadingPanelOutAnim", -1, 0f);
		base.StartCoroutine(this.CloseLoadingPanel());
	}

	private IEnumerator LoadingPanelAnimOutPanel(string SceneName)
	{
		yield return new WaitForSeconds(0.5f);
		string name = SceneManager.GetActiveScene().name;
		this.lastRoutine = base.StartCoroutine("LoadingPanelAnimOutPanel", SceneName);
		if (SceneName == name)
		{
			this.LoadingPanelAnimOut();
			base.StopCoroutine(this.lastRoutine);
		}
		yield break;
	}

	private IEnumerator CloseLoadingPanel()
	{
		yield return new WaitForSeconds(1f);
		this.LoadingPanel.SetActive(false);
		if (PlayerPrefs.GetString("LevelPass") == "Yes" && SceneManager.GetActiveScene().name == "map")
		{
			PlayerPrefs.SetString("LevelPass", "No");
			yield return new WaitForSeconds(5f);
			//MyLogoChangeScript.instance.MovePlayer();
		}
		yield break;
	}

	public GameObject LoadingPanel;

	private Coroutine lastRoutine;
}
