using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SelectLevels : MonoBehaviour
{
    private void Start()
    {
        this.GenerateGrid(0);
    }

    private void GenerateGrid(int genfrom = 0)
    {
        int num = 0;
        this.ClearLevels();
        this.firstShownLevelInGrid = genfrom;
        this.latestFile = this.GetLastLevel();
        int i;
        for (i = genfrom; i < this.latestFile; i++)
        {
            GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.levelPrefab);
            gameObject.GetComponent<Level>().number = i + 1;
            gameObject.transform.SetParent(base.transform);
            gameObject.transform.localPosition = this.startPosition + Vector3.right * (float)(num % this.countInRow) * this.offset.x + Vector3.down * (float)(num / this.countInColumn) * this.offset.y;
            gameObject.transform.localScale = Vector3.one;
            if (num + 1 >= this.countInRow * this.countInColumn)
            {
                break;
            }
            num++;
        }
        if (genfrom == 0)
        {
            this.backButton.gameObject.SetActive(false);
        }
        else if (genfrom > 0)
        {
            this.backButton.gameObject.SetActive(true);
        }
        if (i + 1 >= this.latestFile)
        {
            this.nextButton.gameObject.SetActive(false);
        }
        else
        {
            this.nextButton.gameObject.SetActive(true);
        }
    }

    private void ClearLevels()
    {
        IEnumerator enumerator = base.transform.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                object obj = enumerator.Current;
                Transform transform = (Transform)obj;
                UnityEngine.Object.Destroy(transform.gameObject);
            }
        }
        finally
        {
            IDisposable disposable;
            if ((disposable = (enumerator as IDisposable)) != null)
            {
                disposable.Dispose();
            }
        }
    }

    public void Next()
    {
        this.GenerateGrid(this.firstShownLevelInGrid + this.countInRow * this.countInColumn);
    }

    public void Back()
    {
        this.GenerateGrid(this.firstShownLevelInGrid - this.countInRow * this.countInColumn);
    }

    private int GetLastLevel()
    {
        for (int i = 1; i < 50000; i++)
        {
            TextAsset x = Resources.Load("Levels/" + i) as TextAsset;
            if (x == null)
            {
                Debug.Log(i - 1);
                return i - 1;
            }
        }
        return 0;
    }

    private int latestFile;

    public GameObject levelPrefab;

    public Vector3 startPosition;

    public Vector2 offset;

    public int countInRow = 4;

    public int countInColumn = 4;

    public Button backButton;

    public Button nextButton;

    private int firstShownLevelInGrid;
}
