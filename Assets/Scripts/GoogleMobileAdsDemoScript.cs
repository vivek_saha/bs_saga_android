//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using GoogleMobileAds.Api;
//using InitScriptName;
//using LitJson;
//using UnityEngine;

//public class GoogleMobileAdsDemoScript : MonoBehaviour
//{
//    public static string OutputMessage
//    {
//        set
//        {
//            GoogleMobileAdsDemoScript.outputMessage = value;
//        }
//    }

//    public void Awake()
//    {
//        if (GoogleMobileAdsDemoScript.instance != null)
//        {
//            UnityEngine.Object.Destroy(base.gameObject);
//        }
//    }

//    public void Start()
//    {
//        GoogleMobileAdsDemoScript.instance = this;
//        UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
//        this.url = "http://vasundharaapps.com/artwork_apps/api/AdvertiseNewApplications/15/" + Application.identifier;
//        this.url_icon_ads = "http://vasundharaapps.com/artwork_apps/api/AdvertiseNewApplications/24/" + Application.identifier;
//        this.rewardBasedVideo = RewardBasedVideoAd.Instance;
//        this.rewardBasedVideo.OnAdLoaded += this.HandleRewardBasedVideoLoaded;
//        this.rewardBasedVideo.OnAdFailedToLoad += this.HandleRewardBasedVideoFailedToLoad;
//        this.rewardBasedVideo.OnAdOpening += this.HandleRewardBasedVideoOpened;
//        this.rewardBasedVideo.OnAdStarted += this.HandleRewardBasedVideoStarted;
//        this.rewardBasedVideo.OnAdRewarded += this.HandleRewardBasedVideoRewarded;
//        this.rewardBasedVideo.OnAdClosed += this.HandleRewardBasedVideoClosed;
//        this.rewardBasedVideo.OnAdLeavingApplication += this.HandleRewardBasedVideoLeftApplication;
//        this.RequestRewardBasedVideo();
//        if (PlayerPrefs.GetInt("Ads", 0) == 0)
//        {
//            this.RequestInterstitial();
//        }
//    }

//    private IEnumerator APIcalling()
//    {
//        WWW www = new WWW(this.url);
//        yield return www;
//        if (www.error == null)
//        {
//            this.Processjson(www.text);
//        }
//        yield break;
//    }

//    private IEnumerator APIcalling_ICON_ADS()
//    {
//        WWW www = new WWW(this.url_icon_ads);
//        yield return www;
//        if (www.error == null)
//        {
//            this.ProcesForIcon_ads(www.text);
//        }
//        yield break;
//    }

//    public void Processjson(string JsonString)
//    {
//        JsonData jsonData = JsonMapper.ToObject(JsonString);
//        this.PJD.Status = jsonData["status"].ToString();
//        PlayerPrefs.SetString("Status", this.PJD.Status);
//        if (this.PJD.Status == "1")
//        {
//            for (int i = 0; i < jsonData["data"].Count; i++)
//            {
//                PlayerPrefs.SetString("Applink" + i, jsonData["data"][i]["app_link"].ToString());
//                string text = jsonData["data"][i]["full_thumb_image_landscape"].ToString();
//                text = text.Replace(" ", "%20");
//                string text2 = jsonData["data"][i]["thumb_image"].ToString();
//                text2 = text2.Replace(" ", "%20");
//                if (this.PJD.full_thumb_image.Count != jsonData["data"].Count)
//                {
//                    this.PJD.full_thumb_image.Add(text);
//                    this.PJD.Thumb_Image.Add(text2);
//                }
//                PlayerPrefs.SetString("AppName" + i, jsonData["data"][i]["name"].ToString());
//            }
//        }
//        base.StartCoroutine(this.StoreImage(this.PJD.full_thumb_image, this.PJD.Thumb_Image));
//    }

//    public IEnumerator StoreImage(List<string> Urls, List<string> AppIcon)
//    {
//        for (int i = 0; i < Urls.Count; i++)
//        {
//            if (Urls[i] != string.Empty)
//            {
//                Texture2D texture = new Texture2D(800, 480);
//                WWW www = new WWW(Urls[i]);
//                yield return www;
//                if (www.error != null)
//                {
//                    this.SetData = false;
//                    break;
//                }
//                www.LoadImageIntoTexture(texture);
//                this.PJD.FullImage.Add(Sprite.Create(texture, new Rect(0f, 0f, (float)texture.width, (float)texture.height), new Vector2(0f, 0f)));
//                string path = Application.persistentDataPath + "/texture" + i;
//                File.WriteAllBytes(path, texture.EncodeToJPG());
//                PlayerPrefs.SetInt("totalcount", PlayerPrefs.GetInt("totalcount") + 1);
//                this.SetData = true;
//            }
//        }
//        if (this.SetData)
//        {
//            PlayerPrefs.SetString("HaveAd", "1");
//        }
//        yield break;
//    }

//    public IEnumerator StoreImage_for_icon(List<string> Urls)
//    {
//        for (int i = 0; i < Urls.Count; i++)
//        {
//            if (Urls[i] != string.Empty)
//            {
//                Texture2D texture = new Texture2D(512, 512);
//                WWW www = new WWW(Urls[i]);
//                yield return www;
//                if (www.error != null)
//                {
//                    this.SetData = false;
//                    break;
//                }
//                www.LoadImageIntoTexture(texture);
//                this.PJD_icon.icon.Add(Sprite.Create(texture, new Rect(0f, 0f, (float)texture.width, (float)texture.height), new Vector2(0f, 0f)));
//                string path = Application.persistentDataPath + "/Icon" + i;
//                File.WriteAllBytes(path, texture.EncodeToPNG());
//                this.SetData = true;
//            }
//        }
//        if (this.SetData)
//        {
//            PlayerPrefs.SetString("HaveAd_icon", "1");
//        }
//        yield break;
//    }

//    public void ProcesForIcon_ads(string JsonString)
//    {
//        JsonData jsonData = JsonMapper.ToObject(JsonString);
//        this.PJD_icon.Status = jsonData["status"].ToString();
//        PlayerPrefs.SetString("Status_icon_ads", this.PJD_icon.Status);
//        if (this.PJD_icon.Status == "1")
//        {
//            for (int i = 0; i < jsonData["data"].Count; i++)
//            {
//                PlayerPrefs.SetString("Applink_icon" + i, jsonData["data"][i]["app_link"].ToString());
//                PlayerPrefs.SetString("PackageName" + i, jsonData["data"][i]["package_name"].ToString());
//                string text = jsonData["data"][i]["thumb_image"].ToString();
//                text = text.Replace(" ", "%20");
//                this.PJD_icon.thumb_image.Add(text);
//                PlayerPrefs.SetString("AppName_Icon_Ads" + i, jsonData["data"][i]["name"].ToString());
//            }
//        }
//        base.StartCoroutine(this.StoreImage_for_icon(this.PJD_icon.thumb_image));
//    }

//    public void Update()
//    {
//        this.deltaTime += (Time.deltaTime - this.deltaTime) * 0.1f;
//        if (UnityEngine.Input.GetKeyDown(KeyCode.F))
//        {
//            ScreenCapture.CaptureScreenshot("123.png");
//        }
//    }

//    private AdRequest CreateAdRequest()
//    {
//        return new AdRequest.Builder().AddTestDevice("SIMULATOR").AddTestDevice("060e75b488ab06ecc7868321aaa21868").AddTestDevice("E19949FB5E7C5A28C30A875934AC8181").AddTestDevice("41E9C9F5D1F985FB36C9760EFC8F3916").AddTestDevice("64A3A22A05D9DCDBEC68395FF5048CD1").AddTestDevice("51A49E7B1B359D1999E5C85CE4F54978").AddTestDevice("F9EBC1840023CB004A83005514278635").AddTestDevice("2442EC754FEF046014B26ACCEEAE9C23").AddTestDevice("413FAED40213710754F4D30AC4F60355").AddTestDevice("A7A19E06342F7D3868ABA7863D707BD7").AddTestDevice("78E289C0CB209B06541CB844A1744650").AddTestDevice("0E45853B0874EAA6418891AD964CC470").AddTestDevice("3C8E4AA9C3802D60B83603426D16E430").AddTestDevice("89FAEE279F58CCC3FA1ABC0904E1FCBE").AddTestDevice("74527FD0DD7B0489CFB68BAED192733D").AddTestDevice("BB5542D48765B65F516CF440C3545896").AddTestDevice("E56855A0C493CEF11A7098FE6EA840CB").AddTestDevice("390FED1AE343E9FF9D644C4085C3868E").AddTestDevice("ACFC7B7082B3F3FD4E0AC8E92EA10D53").AddTestDevice("863D8BAE88E209F38FF3C94A0403C776").AddTestDevice("CC24A5104B97346624E2DD6D5E90EA37").AddTestDevice("517048997101BE4535828AC2360582C2").AddTestDevice("8BB4BCB27396AB8ED222B7F902E13420").AddTestDevice("7F3924D7AB776DC5FF2D314CCFEE0EE0").AddTestDevice("F7803FE72A2748F6028D87DC36D7C574").AddTestDevice("8E9BA0470F19FDC3BE855413AA7455E4").AddTestDevice("BB5542D48765B65F516CF440C3545896").AddTestDevice("DD0A309E21D1F24C324C107BE78C1B88").AddTestDevice("2BCBC7FAC3D404C0E93FC9800BD8E2A2").AddTestDevice("2BD06DE73CDDC01BE31408D5A872B147").AddKeyword("game").SetGender(Gender.Male).SetBirthday(new DateTime(1985, 1, 1)).TagForChildDirectedTreatment(false).AddExtra("color_bg", "9B30FF").Build();
//    }

//    private void RequestBanner()
//    {
//        string adUnitId = "ca-app-pub-3940256099942544/6300978111";
//        if (this.bannerView != null)
//        {
//            this.bannerView.Destroy();
//        }
//        this.bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Top);
//        this.bannerView.OnAdLoaded += this.HandleAdLoaded;
//        this.bannerView.OnAdFailedToLoad += this.HandleAdFailedToLoad;
//        this.bannerView.OnAdOpening += this.HandleAdOpened;
//        this.bannerView.OnAdClosed += this.HandleAdClosed;
//        this.bannerView.OnAdLeavingApplication += this.HandleAdLeftApplication;
//        this.bannerView.LoadAd(this.CreateAdRequest());
//    }

//    public void RequestInterstitial()
//    {
//        string adUnitId = "ca-app-pub-4268194143932815/5710058697";
//        if (this.interstitial != null)
//        {
//            this.interstitial.Destroy();
//        }
//        this.interstitial_flag = false;
//        this.interstitial = new InterstitialAd(adUnitId);
//        this.interstitial.OnAdLoaded += this.HandleInterstitialLoaded;
//        this.interstitial.OnAdFailedToLoad += this.HandleInterstitialFailedToLoad;
//        this.interstitial.OnAdOpening += this.HandleInterstitialOpened;
//        this.interstitial.OnAdClosed += this.HandleInterstitialClosed;
//        this.interstitial.OnAdLeavingApplication += this.HandleInterstitialLeftApplication;
//        this.interstitial.LoadAd(this.CreateAdRequest());
//    }

//    public void RequestRewardBasedVideo()
//    {
//        string adUnitId = "ca-app-pub-4268194143932815/6133973799";
//        this.rewardBasedVideo.LoadAd(this.CreateAdRequest(), adUnitId);
//    }

//    public void ShowInterstitial()
//    {
//        if (this.interstitial.IsLoaded())
//        {
//            this.interstitial.Show();
//        }
//        else
//        {
//            MonoBehaviour.print("Interstitial is not ready yet");
//        }
//        this.RequestInterstitial();
//    }

//    public void ShowRewardBasedVideo(int obj)
//    {
//        this.rewadedInt = obj;
//        if (this.rewardBasedVideo.IsLoaded())
//        {
//            this.rewardBasedVideo.Show();
//        }
//        else
//        {
//            MonoBehaviour.print("Reward based video ad is not ready yet");
//            this.RequestRewardBasedVideo();
//        }
//    }

//    public void HandleAdLoaded(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleAdLoaded event received");
//    }

//    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
//    {
//        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: " + args.Message);
//    }

//    public void HandleAdOpened(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleAdOpened event received");
//    }

//    public void HandleAdClosed(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleAdClosed event received");
//    }

//    public void HandleAdLeftApplication(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleAdLeftApplication event received");
//    }

//    public void HandleInterstitialLoaded(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleInterstitialLoaded event received");
//    }

//    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
//    {
//        this.interstitial_flag = true;
//        MonoBehaviour.print("HandleInterstitialFailedToLoad event received with message: " + args.Message);
//    }

//    public void HandleInterstitialOpened(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleInterstitialOpened event received");
//    }

//    public void HandleInterstitialClosed(object sender, EventArgs args)
//    {
//        this.RequestInterstitial();
//        MonoBehaviour.print("HandleInterstitialClosed event received");
//    }

//    public void HandleInterstitialLeftApplication(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleInterstitialLeftApplication event received");
//    }

//    public void HandleNativeExpressAdLoaded(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleNativeExpressAdAdLoaded event received");
//    }

//    public void HandleNativeExpresseAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
//    {
//        MonoBehaviour.print("HandleNativeExpressAdFailedToReceiveAd event received with message: " + args.Message);
//    }

//    public void HandleNativeExpressAdOpened(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleNativeExpressAdAdOpened event received");
//    }

//    public void HandleNativeExpressAdClosed(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleNativeExpressAdAdClosed event received");
//    }

//    public void HandleNativeExpressAdLeftApplication(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleNativeExpressAdAdLeftApplication event received");
//    }

//    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
//    {
//        this.RewardedAds_flag = true;
//        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
//    }

//    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
//    {
//        MonoBehaviour.print("HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
//    }

//    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
//    {
//        this.RewardedAds_flag = false;
//        this.watchFullvideo = false;
//        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
//    }

//    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
//    }

//    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
//    {
//        this.RequestRewardBasedVideo();
//        if (!this.watchFullvideo)
//        {
//        }
//        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
//    }

//    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
//    {
//        string type = args.Type;
//        MonoBehaviour.print("HandleRewardBasedVideoRewarded event received for " + args.Amount.ToString() + " " + type);
//        InitScript.Instance.AddGems(3);
//        this.watchFullvideo = true;
//    }

//    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
//    {
//        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
//    }

//    public GameObject IconAd_1;

//    public GameObject IconAd_2;

//    public bool SetData;

//    private string url;

//    private string url_icon_ads;

//    public bool watchFullvideo;

//    public int rewadedInt;

//    public static GoogleMobileAdsDemoScript instance;

//    public bool RewardedAds_flag;

//    private BannerView bannerView;

//    public InterstitialAd interstitial;

//    public RewardBasedVideoAd rewardBasedVideo;

//    public bool rewaded_flag = true;

//    public bool interstitial_flag = true;

//    private float deltaTime;

//    private static string outputMessage = string.Empty;

//    private GoogleMobileAdsDemoScript.ParseJsonData PJD = new GoogleMobileAdsDemoScript.ParseJsonData();

//    private GoogleMobileAdsDemoScript.ParseJsonData_icon_ads PJD_icon = new GoogleMobileAdsDemoScript.ParseJsonData_icon_ads();

//    public class ParseJsonData
//    {
//        public string Status;

//        public List<string> full_thumb_image = new List<string>();

//        public List<string> Applink = new List<string>();

//        public List<Sprite> FullImage = new List<Sprite>();

//        public List<string> Thumb_Image = new List<string>();
//    }

//    public class ParseJsonData_icon_ads
//    {
//        public string Status;

//        public List<string> thumb_image = new List<string>();

//        public List<string> Applink = new List<string>();

//        public List<Sprite> icon = new List<Sprite>();
//    }
//}
