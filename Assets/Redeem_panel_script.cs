﻿using UnityEngine;

public class Redeem_panel_script : MonoBehaviour
{
    public static Redeem_panel_script Instance;


    public GameObject skin_redeem_but;

    public GameObject mlbb_panel, freefire_panel, roblox_panel;


    [Header("mlbb")]
    [Space]
    public GameObject mlbb_content;


    [Header("FreeFire")]
    [Space]
    public GameObject freefire_content;


    [Header("Roblox")]
    [Space]
    public GameObject roblox_content;



    private void Awake()
    {
        Instance = this;
    }

    public void Onclick_panel_change_(GameObject tp)
    {
        mlbb_panel.SetActive(false);
        freefire_panel.SetActive(false);
        roblox_panel.SetActive(false);
        tp.SetActive(true);
    }


    public void generate_but()
    {

        foreach (Transform a in freefire_content.transform)
        {
            Destroy(a.gameObject);
        }
        //ff
        for (int i = 0; i < Setting_API.Instance.product_id_redeem_FF.Length; i++)
        {
            GameObject temp = Instantiate(skin_redeem_but);
            temp.transform.SetParent(freefire_content.transform);
            temp.transform.localScale = Vector3.one;
            
            temp.GetComponent<skin_redeem_but_script>().Skin_Name.text = Setting_API.Instance.name_redeem_FF[i];
            temp.GetComponent<skin_redeem_but_script>().Skin_description.text = Setting_API.Instance.desc_redeem_FF[i];
            temp.GetComponent<skin_redeem_but_script>().product_id = Setting_API.Instance.product_id_redeem_FF[i];
            temp.GetComponent<skin_redeem_but_script>().skinImage_link = Setting_API.Instance.img_redeem_FF[i];
            temp.GetComponent<skin_redeem_but_script>().frag_limit = int.Parse(Setting_API.Instance.frag_redeem_FF[i]);
            
            player_prefs_adder_checker(Setting_API.Instance.name_redeem_FF[i]);
            temp.GetComponent<skin_redeem_but_script>().set_fragment_fill();
            //temp.GetComponent<skin_redeem_but_script>().Download_image();
            temp.GetComponent<skin_redeem_but_script>().button_Click();
        }


        foreach (Transform a in mlbb_content.transform)
        {
            Destroy(a.gameObject);
        }

        ////MLBB
        for (int i = 0; i < Setting_API.Instance.product_id_redeem_MLBB.Length; i++)
        {
            GameObject temp = Instantiate(skin_redeem_but);
            temp.transform.SetParent(mlbb_content.transform);
            temp.transform.localScale = Vector3.one;
           
            temp.GetComponent<skin_redeem_but_script>().Skin_Name.text = Setting_API.Instance.name_redeem_MLBB[i];
            temp.GetComponent<skin_redeem_but_script>().Skin_description.text = Setting_API.Instance.desc_redeem_MLBB[i];
            temp.GetComponent<skin_redeem_but_script>().product_id = Setting_API.Instance.product_id_redeem_MLBB[i];
            temp.GetComponent<skin_redeem_but_script>().skinImage_link = Setting_API.Instance.img_redeem_MLBB[i];
            temp.GetComponent<skin_redeem_but_script>().frag_limit = int.Parse(Setting_API.Instance.frag_redeem_MLBB[i]);

            player_prefs_adder_checker(Setting_API.Instance.name_redeem_MLBB[i]);
            temp.GetComponent<skin_redeem_but_script>().set_fragment_fill();
            //temp.GetComponent<skin_redeem_but_script>().Download_image();
            temp.GetComponent<skin_redeem_but_script>().button_Click();
        }


        foreach (Transform a in roblox_content.transform)
        {
            Destroy(a.gameObject);
        }

        ////Roblox
        for (int i = 0; i < Setting_API.Instance.product_id_redeem_ROB.Length; i++)
        {
            GameObject temp = Instantiate(skin_redeem_but);
            temp.transform.SetParent(roblox_content.transform);
            temp.transform.localScale = Vector3.one;
 
            temp.GetComponent<skin_redeem_but_script>().Skin_Name.text = Setting_API.Instance.name_redeem_ROB[i];
            temp.GetComponent<skin_redeem_but_script>().Skin_description.text = Setting_API.Instance.desc_redeem_ROB[i];
            temp.GetComponent<skin_redeem_but_script>().product_id = Setting_API.Instance.product_id_redeem_ROB[i];
            temp.GetComponent<skin_redeem_but_script>().skinImage_link = Setting_API.Instance.img_redeem_ROB[i];
            temp.GetComponent<skin_redeem_but_script>().frag_limit = int.Parse(Setting_API.Instance.frag_redeem_ROB[i]);

            player_prefs_adder_checker(Setting_API.Instance.name_redeem_FF[i]);
            temp.GetComponent<skin_redeem_but_script>().set_fragment_fill();
            //temp.GetComponent<skin_redeem_but_script>().Download_image();
            temp.GetComponent<skin_redeem_but_script>().button_Click();
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }



    public void player_prefs_adder_checker(string KeyName)
    {
        if (PlayerPrefs.HasKey(KeyName))
        {

            Debug.Log("The key " + KeyName + " exists");
        }
        else
        {

            PlayerPrefs.SetInt(KeyName, 0);
            Debug.Log("The key " + KeyName + " does not exist");
        }
    }
}
