﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bg_canvas_script : MonoBehaviour
{


    public Sprite[] BG_sprites;
    public Sprite[] BG_front_sprites;

    public Image BG_image;

    public Image BG_front;
    // Start is called before the first frame update
    void Start()
    {
        //ipad and iphone resolution set
        //if (((float)Screen.height / (float)Screen.width) < 1.7)
        //{
        //    BG_front.sprite = BG_front_sprites[1];
        //}
        //else
        //{
        //    BG_front.sprite = BG_front_sprites[0];
        //}
        if (((float)Screen.height / (float)Screen.width) < 1.7)
        {
            //iphone x
            BG_front.sprite = BG_front_sprites[1];
        }
        else if (((float)Screen.height / (float)Screen.width) > 2)
        {
            //ipad
            BG_front.sprite = BG_front_sprites[2];
        }
        else
        {
            //iphone
            BG_front.sprite = BG_front_sprites[0];
        }


    }

    // Update is called once per frame
    void Update()
    {

    }
}
