﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Withdraw_panel_script : MonoBehaviour
{
    public static Withdraw_panel_script Instance;


    public GameObject Diamond_but, robux_diamond_but;

    public GameObject mlbb_panel, freefire_panel, roblox_panel;

    public Sprite[] diamond_sprite;


    [Header("mlbb")]
    [Space]
    public GameObject mlbb_content;


    [Header("FreeFire")]
    [Space]
    public GameObject freefire_content;


    [Header("Roblox")]
    [Space]
    public GameObject roblox_content;



    private void Awake()
    {
        Instance = this;
    }

    public void Onclick_panel_change_(GameObject tp)
    {
        mlbb_panel.SetActive(false);
        freefire_panel.SetActive(false);
        roblox_panel.SetActive(false);
        tp.SetActive(true);
    }


    public void generate_but()
    {

        foreach(Transform a in freefire_content.transform)
        {
            Destroy(a.gameObject);
        }
        //ff
        for (int i = 0; i < Setting_API.Instance.product_id_withdraw_FF.Length; i++)
        {
            GameObject temp = Instantiate(Diamond_but);
            temp.transform.SetParent(freefire_content.transform);
            temp.transform.localScale = Vector3.one;
            temp.GetComponent<withdraw_but_script>().wth = withdraw_type.Diamond;
            temp.GetComponent<withdraw_but_script>().diamond_image.sprite = diamond_sprite[1];
            temp.GetComponent<withdraw_but_script>().Diamond_value.text = Setting_API.Instance.game_dia_withdraw_FF[i];
            temp.GetComponent<withdraw_but_script>().cost_value.text = Setting_API.Instance.dia_withdraw_FF[i];
            temp.GetComponent<withdraw_but_script>().product_id = Setting_API.Instance.product_id_withdraw_FF[i];
            temp.GetComponent<withdraw_but_script>().button_Click();
        }


        foreach (Transform a in mlbb_content.transform)
        {
            Destroy(a.gameObject);
        }

        //MLBB
        for (int i = 0; i < Setting_API.Instance.product_id_withdraw_MLBB.Length; i++)
        {
            //Debug.Log(i);
            GameObject temp = Instantiate(Diamond_but);
            temp.transform.SetParent(mlbb_content.transform);
            temp.transform.localScale = Vector3.one;
            temp.GetComponent<withdraw_but_script>().wth = withdraw_type.Diamond;
            temp.GetComponent<withdraw_but_script>().diamond_image.sprite = diamond_sprite[0];
            temp.GetComponent<withdraw_but_script>().Diamond_value.text = Setting_API.Instance.game_dia_withdraw_MLBB[i];
            temp.GetComponent<withdraw_but_script>().cost_value.text = Setting_API.Instance.dia_withdraw_MLBB[i];
            temp.GetComponent<withdraw_but_script>().product_id = Setting_API.Instance.product_id_redeem_MLBB[i];
            temp.GetComponent<withdraw_but_script>().button_Click();
        }



        foreach (Transform a in roblox_content.transform)
        {
            Destroy(a.gameObject);
        }
        //Roblox
        for (int i = 0; i < Setting_API.Instance.product_id_withdraw_ROB.Length; i++)
        {
            GameObject temp = Instantiate(robux_diamond_but);
            temp.transform.SetParent(roblox_content.transform);
            temp.transform.localScale = Vector3.one;
            temp.GetComponent<withdraw_but_script>().wth = withdraw_type.Robux_Diamond;
            //temp.GetComponent<withdraw_but_script>().diamond_image.sprite = diamond_sprite[0];
            temp.GetComponent<withdraw_but_script>().Robux_value.text = Setting_API.Instance.game_dia_withdraw_ROB[i];
            temp.GetComponent<withdraw_but_script>().cost_value.text = Setting_API.Instance.dia_withdraw_ROB[i];
            temp.GetComponent<withdraw_but_script>().product_id = Setting_API.Instance.product_id_redeem_ROB[i];
            temp.GetComponent<withdraw_but_script>().button_Click();
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
